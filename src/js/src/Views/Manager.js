import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Route } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { LinkContainer } from "react-router-bootstrap";
import Reports from "./Reports";
import RefreshToken from "../components/RefreshToken";
import Header from "./Header";
import OrdersList from "../components/OrdersList";
import axios from "axios";
import RecipeList from "../components/RecipesList";
import DishListClient from "../components/DishListClient";
import { Row, Col } from "react-bootstrap";
import Users from "../components/Users";
import Ingredients from "./Ingredients";
import Dishes from "./Dishes";

class Manager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: false,
      // rendering: false,
      orderId: 0,
      dishlist: [],
      recipeList: [],
      usersList: [],

      // orderId: 0,
      orderValue: 0,
      userName: window.sessionStorage.getItem("login"),
    };
    this.setOrderId = this.setOrderId.bind(this);
    this.setOrderValue = this.setOrderValue.bind(this);
    // this.switchHandle = this.switchHandle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    // this.handleValueChange = this.handleValueChange.bind(this);
    this.handleValueChange = this.handleValueChange.bind(this);
    // this.switchHandle = this.switchHandle.bind(this);
  }

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      // baseUrl: "/",
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };

    axios
      .get("/api/dish", config)
      .then((res) => {
        this.setState({ dishlist: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
    axios
      .get("/api/recipe", config)
      .then((res) => {
        this.setState({ recipeList: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
    axios
      .get("/api/user/all", config)
      .then((res) => {
        this.setState({ usersList: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
  }

  selectTable = () => {
    let selected = [];
    let test = this.state.usersList;
    selected.push(<option></option>);

    for (let i = 0; i < test.length; i++) {
      if (test[i].authority === "ROLE_CUSTOMER")
        selected.push(<option>{test[i].userName}</option>);
    }
    return selected;
  };
  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleValueChange() {
    this.setState({ value: !this.state.value });
    window.location.reload(false);
  }
  setOrderId(newOrderId) {
    this.state.orderId = newOrderId;
  }

  setOrderValue(totalOrderValue) {
    this.state.orderValue = totalOrderValue;
  }

  returnNavBar() {
    return (
      <div>
        <div>
          <Navbar bg="light" expand="lg">
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
                <LinkContainer to={this.props.match.path}>
                  <Nav.Link>Menu</Nav.Link>
                </LinkContainer>
                <LinkContainer to={"/Manager/Orders"}>
                  <Nav.Link>Orders</Nav.Link>
                </LinkContainer>
                <LinkContainer to={"/Manager/Recipes"}>
                  <Nav.Link>Recipes</Nav.Link>
                </LinkContainer>
                <LinkContainer to={"/Manager/Dishes"}>
                  <Nav.Link>Dishes</Nav.Link>
                </LinkContainer>
                <LinkContainer to={"/Manager/Ingredients"}>
                  <Nav.Link>Ingredients</Nav.Link>
                </LinkContainer>
                <LinkContainer to={"/Manager/Users"}>
                  <Nav.Link>Users</Nav.Link>
                </LinkContainer>
                <LinkContainer to={"/Manager/Reports"}>
                  <Nav.Link>Reports</Nav.Link>
                </LinkContainer>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <Header />
        {this.returnNavBar()}
        <div>
          <Route
            exact
            path={this.props.match.path}
            render={(props) => (
              <div>
                <Row>
                  <Col xs={{ span: 6 }}>
                    <Form>
                      <Form.Group controlId="exampleForm.SelectCustom">
                        <Form.Label>Choose table no.</Form.Label>
                        <Form.Control
                          name="userName"
                          defaultValue={this.selectTable()[0]}
                          as="select"
                          custom
                          onChange={this.handleChange}
                        >
                          {this.selectTable()}
                        </Form.Control>
                      </Form.Group>
                    </Form>
                  </Col>
                </Row>
                <DishListClient
                  {...props}
                  orderId={this.state.orderId}
                  dishlist={this.state.dishlist}
                  recipeList={this.state.recipeList}
                  userName={this.state.userName}
                  isOrderPlaced={this.handleValueChange}
                  setOrderId={this.setOrderId}
                  setOrderValue={this.setOrderValue}
                />
              </div>
            )}
          />
          <Route
            path={`${this.props.match.path}/Orders`}
            render={(props) => (
              <OrdersList {...props} dishlist={this.state.dishlist} />
            )}
          />
          <Route
            path={`${this.props.match.path}/Recipes`}
            render={(props) => (
              <RecipeList {...props} dishlist={this.state.dishlist} />
            )}
          />
          <Route
            path={`${this.props.match.path}/Dishes`}
            render={(props) => (
              <Dishes
                {...props}
                dishlist={this.state.dishlist}
                recipeList={this.state.recipeList}
              />
            )}
          />
          <Route
            path={`${this.props.match.path}/Ingredients`}
            component={Ingredients}
          />
          <Route
            path={`${this.props.match.path}/Users`}
            render={(props) => (
              <Users {...props} usersList={this.state.usersList} />
            )}
          />
          <Route
            path={`${this.props.match.path}/Reports`}
            component={Reports}
          />
        </div>
      </div>
    );
  }
}

export default Manager;
