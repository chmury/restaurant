import React, { Component } from "react";
import axios from "axios";
import { Row, Col, Button } from "react-bootstrap";
import RefreshToken from "../components/RefreshToken";

export default class OrderSummary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order: [],
      orderedDishesName: "",
      time: Date.now(),
    };
    this.findDishesNames = this.findDishesNames.bind(this);
    this.fetchFromApi = this.fetchFromApi.bind(this);
  }

  componentDidMount() {
    this.interval = setInterval(
      () => this.setState({ time: Date.now() }),
      10000
    );
    console.log("compdidmount");
    this.fetchFromApi();
  }
  componentWillUpdate() {
    setTimeout(() => {
      console.log("compwillupdate");
      this.fetchFromApi();
    }, 20000);
  }

  fetchFromApi() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    axios
      .get("api/order/" + this.props.orderId, config)
      .then((res) => {
        this.setState({ order: res.data });
        this.findDishesNames();
        // console.log(this.state.orderedDishesName);
      })
      .catch(() => {
        RefreshToken();
      });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    // this.jakas();
  }

  findDishesNames() {
    // console.log(this.state.order);
    let arrayDishes = this.state.order.orderedDishes;
    let arrayD = this.props.dishlist;
    let finded = "";
    for (let i = 0; i < arrayDishes.length; i++) {
      for (let j = 0; j < arrayD.length; j++) {
        if (arrayDishes[i] === arrayD[j].dishId) {
          finded += arrayD[j].dishName + ", ";
        }
      }
    }
    // console.log(finded);
    finded = finded.substr(0, finded.length - 2);
    this.setState({ orderedDishesName: finded });
  }
  orderPlaced(id) {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
        },
        proxy: {
          host: "localhost",
          port: 8080,
        },
      };
      const response = axios
        .post("api/order/status/" + id + "/Oplacone", {}, config)
        .catch(() => {
          RefreshToken();
        });
      alert("Dziękujemy za opłacenie zamówienia");
      window.location.reload(false);
    } catch (e) {
      RefreshToken();
      console.log(`Axios request failed: ${e}`);
    }
    // window.location.reload(false);
  }

  buttonRender(order) {
    if (order.orderStatus === "Wydane") {
      return (
        <Button
          block
          variant="success"
          onClick={() => {
            this.orderPlaced(order.orderId);
          }}
        >
          Payment
        </Button>
      );
    }
    return null;
  }

  render() {
    console.log(this.state.time);
    console.log("renderuje");
    console.log(this.state.order);
    return (
      <div>
        <h1>Your order summary</h1>

        <Row>
          <Col>#</Col>
          <Col>Produkty</Col>
          <Col>Kwota</Col>
          <Col>Status</Col>
        </Row>
        <Row>
          <Col>{this.state.order.orderId}</Col>
          <Col>{this.state.orderedDishesName}</Col>
          <Col>{this.props.orderValue}</Col>
          <Col>{this.state.order.orderStatus}</Col>
        </Row>
        <Row>
          <Col xs={{ span: 4, offset: 4 }}>
            {this.buttonRender(this.state.order)}
          </Col>
        </Row>
      </div>
    );
  }
}
