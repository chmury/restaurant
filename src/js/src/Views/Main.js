import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import LoginPage from "./LoginPage";
import Customer from "./Customer";
import Waiter from "./Waiter";
import Kitchen from "./Kitchen";
import Manager from "./Manager";
import OrderSummary from "./OrderSummary";

export default class Main extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path={"/Menu"} component={Customer} />
          {/* <Route path={"/Manager"} component={Manager} /> */}
          <Route path={"/Manager"} component={Manager} />
          {/* <Route path={"/Manager"} component={Waiter} hash="#menu" /> */}

          <Route path={"/Waiter"} component={Waiter} />
          <Route path={"/Kitchen"} component={Kitchen} />
          <Route exact path={"/"} component={LoginPage} />
        </Switch>

        {/* <Router history = {createBrowserHistory()} > */}
      </Router>
    );
  }
}
