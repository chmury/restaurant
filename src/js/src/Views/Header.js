import React, { Component } from "react";
import Image from "react-bootstrap/Image";
import Button from "react-bootstrap/Button";
import history from "../History";

import logo from "../images/logo.png";

import "../css/header.css";

export default class Header extends Component {
  logOutUser() {
    // console.log("dziala");
    sessionStorage.removeItem("ROLE");
    sessionStorage.removeItem("login");
    sessionStorage.removeItem("jwt");
    sessionStorage.removeItem("jwt_refresh");
    history.push("/");
    window.location.reload();
  }

  render() {
    return (
      <div className="mainDiv">
        <div className="logoDiv">
          <Image src={logo} className="logoImage" />
        </div>
        <div className="titleDiv">Restaurant</div>
        <div className="userName">
          <span className="cUser">Current user</span>
          <br />
          <b>{window.sessionStorage.getItem("login")}</b>
        </div>
        <div className="logOut">
          <Button variant="danger" onClick={() => this.logOutUser()}>
            Logout
          </Button>
        </div>
        <div style={{ clear: "both" }}></div>
      </div>
    );
  }
}
