import React, { Component } from "react";
import axios from "axios";
import { Table, Row, Col } from "react-bootstrap";
import RefreshToken from "../components/RefreshToken";

export default class Reports extends Component {
  constructor(props) {
    super(props);
    this.state = {
      month: [],
      orderAmount: [],
      dishesAmount: [],
      value: [],
    };
  }

  componentDidMount() {
    var XMLParser = require("react-xml-parser");

    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    axios
      .get("/api/report", config)
      .then((res) => {
          var xml = new XMLParser().parseFromString(res.data);

        this.setState({
          month: xml.getElementsByTagName("month"),
          orderAmount: xml.getElementsByTagName("orderAmount"),
          dishesAmount: xml.getElementsByTagName("dishesAmount"),
          value: xml.getElementsByTagName("value"),
        });
       
      })
      .catch((e) => {
        RefreshToken();
      });
  }

  renderTable() {
    let table = [];
    
    for (let i = 0; i < this.state.month.length; i++) {
      table.push(
        <tr>
          <td>{i + 1}</td>
          <td>{this.state.month[i].value}</td>
          <td>{this.state.orderAmount[i].value}</td>
          <td>{this.state.dishesAmount[i].value}</td>
          <td>{this.state.value[i].value}</td>
        </tr>
      );
    }
    return table;
  }

  render() {
    return (
      <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
        <thead>
          <tr>
            <th style={{ width: "5%" }}>#</th>
            <th style={{ width: "25%" }}>Date</th>
            <th style={{ width: "20%" }}>Orders amount</th>
            <th style={{ width: "20%" }}>Ordered dishes amount</th>
            <th style={{ width: "20%" }}>Total income [PLN]</th>
          </tr>
        </thead>
        <tbody>
          {this.renderTable()}

        </tbody>
      </Table>
    );
  }
}
