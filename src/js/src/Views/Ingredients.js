import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Button, Row, Col, Alert } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";
import RefreshToken from "../components/RefreshToken";

export default class Ingredients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ingredientsList: [],
      ingredientName: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.addNewIngredient = this.addNewIngredient.bind(this);
  }

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
     
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    axios
      .get("/api/ingredient", config)
      .then((res) => {
        this.setState({ ingredientsList: res.data });
      })
      .catch((e) => {
      
        RefreshToken();
      });
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  async addNewIngredient() {
    
      const config = {
        headers: {
          Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
        },
        proxy: {
          host: "localhost",
          port: 8080,
        },
      };
      const response = await axios
        .post(
          "/api/ingredient",
          {
            ingredientName: this.state.ingredientName,
          },
          config
        )
        .catch((e) => {
          alert("wyjabajajo sie")
          alert(e)
          RefreshToken();
        });
      console.log("Returned data:", response);
      window.location.reload(false);
  
   
  }

  async removeIngredient(ingredientId) {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    const response = await axios
      .delete("/api/ingredient/remove/" + ingredientId, config)
      .catch(() => {
        RefreshToken();
      });

    window.location.reload(false);
  }

  render() {
    return (
      <Row>
        <Col md={{ span: 7 }} className="ingredientTableCol">
          <h4>Ingredients list</h4>
          <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
            <thead>
              <tr>
                <th style={{ width: "5%" }}>#</th>
                <th rowSpan="2" style={{ width: "65%" }}>
                  Ingredient name
                </th>
                <th style={{ width: "10%" }}>Remove</th>
              </tr>
            </thead>

            <tbody>
              {this.state.ingredientsList &&
                this.state.ingredientsList.map((ingredient, i) => (
                  <tr key={i}>
                    <td>{ingredient.ingredientId}</td>
                    {/* <td>{i + 1}</td> */}
                    <td>{ingredient.ingredientName}</td>
                    <td>
                      <Button
                        variant="warning"
                        onClick={() =>
                          this.removeIngredient(ingredient.ingredientId)
                        }
                      >
                        -
                      </Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </Col>
        <Col md={{ span: 3, offset: 1 }} className="ingredientTableCol">
          <h4>Add new ingredient</h4>
          <Form>
            <Form.Group controlId="formBasicIngredient">
              <Form.Label>Ingredient name</Form.Label>
              <Form.Control
                type="text"
                name="ingredientName"
                placeholder="Enter ingredient name here"
                onChange={this.handleChange}
              />
            </Form.Group>
            <Button
              variant="primary"
              block
              type="submit"
              onClick={() => {
                this.addNewIngredient();
              }}
            >
              Create
            </Button>
          </Form>
        </Col>
      </Row>
    );
  }
}
