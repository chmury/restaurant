import React, { Component } from "react";
import axios from "axios";
import { Row, Col, Form, Button } from "react-bootstrap";
import Image from "react-bootstrap/Image";

import ViewControl from "../functions/ViewControl";

import logo from "../images/logo.png";

class LoginPage extends Component {
  constructor() {
    super();
    this.state = {
      login: "",
      pass: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  }

  async handleClick() {
    try {
      await axios
        .post(
          "oauth/token?grant_type=password&username=" +
            this.state.login +
            "&password=" +
            this.state.pass,
          null,
          {
            auth: {
              username: "admin",
              password: "admin",
            },
            proxy: {
              host: "localhost",
              port: 8080,
            },
          }
        )
        .then((res) => {
          window.sessionStorage.setItem("jwt", res.data.access_token);
          window.sessionStorage.setItem("jwt_refresh", res.data.refresh_token);
        })
        .catch((e) => {
          alert("Incorrect credentials!");
        });

      window.sessionStorage.setItem("login", this.state.login);
      window.sessionStorage.setItem("ROLE", "ROLE_CUSTOMER");
    } catch (e) {
      console.log(`Axios request failed: ${e}`);
      // alert("Incorrect credentials!");
    }
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };

    axios.get("api/user/role/" + this.state.login, config).then((res) => {
      window.sessionStorage.setItem("ROLE", res.data.userRole);
      ViewControl();
    });
  }

  render() {
    return (
      <div>
        <Row style={{ marginBottom: "2%", marginTop: "15%" }}>
          <Col xs={{ span: 6, offset: 3 }} sm={{ span: 4, offset: 4 }}>
            <Image src={logo} fluid />
          </Col>
        </Row>
        <Form>
          <Row>
            <Col xs={{ span: 6, offset: 3 }} md={{ span: 4, offset: 4 }}>
              <Form.Group controlId="login">
                <Form.Label> Nazwa użytkownika</Form.Label>
                <Form.Control
                  type="text"
                  name="login"
                  placeholder="Wpisz nazwę użytkownika"
                  onChange={this.handleChange}
                />
              </Form.Group>
            </Col>
            <Col xs={{ span: 6, offset: 3 }} md={{ span: 4, offset: 4 }}>
              <Form.Group controlId="pass">
                <Form.Label> Hasło </Form.Label>
                <Form.Control
                  type="password"
                  name="pass"
                  placeholder="Wpisz swoje hasło"
                  onChange={this.handleChange}
                />
              </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 4, offset: 4 }} lg={{ span: 2, offset: 5 }}>
              <Button block variant="primary" onClick={this.handleClick}>
                Zaloguj się
              </Button>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}
export default LoginPage;

// import React, { Component } from "react";
// import axios from "axios";
// import { Row, Col, Form, Button } from "react-bootstrap";

// import ViewControl from "../ViewControl";
// import RoleRequest from "../RoleRequest";

// class LoginPage extends Component {
//   constructor() {
//     super();
//     this.state = {
//       login: "",
//       pass: "",
//       token: [],
//     };
//     this.handleChange = this.handleChange.bind(this);
//     this.handleClick = this.handleClick.bind(this);
//   }

//   handleChange(event) {
//     const { name, value } = event.target;
//     this.setState({ [name]: value });
//   }

//   async handleClick() {
//     try {
//       let headers = new Headers();

// headers.append('Authorization', 'Basic ' + btoa('admin' + ":" + 'admin'));
//       var basicAuth = "Basic " + "admin:admin";
//       await axios
//         .post(
//           "oauth/token?grant_type=password&username=" +
//             this.state.login +
//             "&password=" +
//             this.state.pass,
//           null,
//           {
//             auth: {
//               username: "admin",
//               password: "admin",
//             },
//             proxy: {
//               host: 'localhost',
//               port: 8080
//             }
//           }
//         )
//         .then((res) => {
//           this.setState({ token: res.data });
//         });

//       window.sessionStorage.setItem("jwt", this.state.token.access_token);
//       window.sessionStorage.setItem("login", this.state.login);
//       RoleRequest();
//       ViewControl();
//       // window.location.reload();
//     } catch (e) {
//       console.log(`Axios request failed: ${e}`);
//     }
//   }

//   render() {
//     return (
//       <Form>
//         <Row>
//           <Col md-4>
//             <Form.Group controlId="login">
//               <Form.Label>Nazwa użytkownika</Form.Label>
//               <Form.Control
//                 type="text"
//                 name="login"
//                 placeholder="Wpisz nazwę użytkownika"
//                 onChange={this.handleChange}
//               />
//             </Form.Group>
//           </Col>
//           <Col md-4>
//             <Form.Group controlId="pass">
//               <Form.Label>Hasło</Form.Label>
//               <Form.Control
//                 type="password"
//                 name="pass"
//                 placeholder="Wpisz swoje hasło"
//                 onChange={this.handleChange}
//               />
//             </Form.Group>
//           </Col>
//         </Row>
//         <Row>
//           <Col sm>
//             <Button variant="primary" onClick={this.handleClick}>
//               Zaloguj się
//             </Button>
//           </Col>
//         </Row>
//       </Form>
//     );
//   }
// }
// export default LoginPage;
