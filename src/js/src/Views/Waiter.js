import React, { Component } from "react";
import axios from "axios";
import Form from "react-bootstrap/Form";
import { Row, Col } from "react-bootstrap";

import DishListClient from "../components/DishListClient";
import OrdersList from "../components/OrdersList";
import Header from "./Header";
import RefreshToken from "../components/RefreshToken";

class Waiter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: false,
      rendering: false,
      dishlist: [],
      recipeList: [],
      usersList: [],

      orderId: 0,
      orderValue: 0,
      userName: window.sessionStorage.getItem("login"),
    };

    this.setOrderId = this.setOrderId.bind(this);
    this.setOrderValue = this.setOrderValue.bind(this);
    this.switchHandle = this.switchHandle.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleValueChange = this.handleValueChange.bind(this);
  }

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    try {
      axios
        .get("/api/dish", config)
        .then((res) => {
          this.setState({ dishlist: res.data });
        })
        .catch(() => {
          RefreshToken();
        });
      axios
        .get("/api/recipe", config)
        .then((res) => {
          this.setState({ recipeList: res.data });
        })
        .catch(() => {
          RefreshToken();
        });
      axios
        .get("/api/user/all", config)
        .then((res) => {
          this.setState({ usersList: res.data });
        })
        .catch(() => {
          RefreshToken();
        });
    } catch (e) {
      alert("401");
      RefreshToken();
    }
  }

  handleValueChange() {
    this.setState({ value: !this.state.value });
    window.location.reload(false);
    // console.log(this.state.value);
  }

  setOrderId(newOrderId) {
    this.state.orderId = newOrderId;
  }

  setOrderValue(totalOrderValue) {
    this.state.orderValue = totalOrderValue;
  }

  selectTable = () => {
    let selected = [];
    let test = this.state.usersList;
    selected.push(<option></option>);

    for (let i = 0; i < test.length; i++) {
      if (test[i].authority === "ROLE_CUSTOMER")
        selected.push(<option>{test[i].userName}</option>);
    }
    return selected;
  };

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  switchHandle() {
    this.setState({ rendering: !this.state.rendering });
  }

  render() {
    if (!this.state.rendering) {
      return (
        <div>
          <Row>
            <Col>
              <Header />
            </Col>
          </Row>
          <Row>
            <Col>
              <Form>
                <Form.Check
                  type="switch"
                  id="custom-switch"
                  label="Place an order | Track orders"
                  onChange={this.switchHandle}
                />
              </Form>
            </Col>
          </Row>
          <Row>
            <Col xs={{ span: 6 }}>
              <Form>
                <Form.Group controlId="exampleForm.SelectCustom">
                  <Form.Label>Choose table no.</Form.Label>
                  <Form.Control
                    name="userName"
                    defaultValue={this.selectTable()[0]}
                    as="select"
                    custom
                    onChange={this.handleChange}
                  >
                    {this.selectTable()}
                  </Form.Control>
                </Form.Group>
              </Form>
            </Col>
          </Row>
          <Row>
            <Col>
              <DishListClient
                orderId={this.state.orderId}
                dishlist={this.state.dishlist}
                recipeList={this.state.recipeList}
                userName={this.state.userName}
                isOrderPlaced={this.handleValueChange}
                setOrderId={this.setOrderId}
                setOrderValue={this.setOrderValue}
              />
            </Col>
          </Row>
        </div>
      );
    } else {
      return (
        <div>
          <Row>
            <Col>
              <Header />
            </Col>
          </Row>
          <Row>
            <Col>
              <Form>
                <Form.Check
                  type="switch"
                  id="custom-switch"
                  label="Place an order | Track orders"
                  onChange={this.switchHandle}
                />
              </Form>
            </Col>
          </Row>
          <OrdersList dishlist={this.state.dishlist} />
        </div>
      );
    }
  }
}

export default Waiter;
