import React, { Component } from "react";
import DishListClient from "../components/DishListClient";
import Header from "./Header";
import OrderSummary from "./OrderSummary";
import axios from "axios";
import RefreshToken from "../components/RefreshToken";
export default class Customer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: false,
      dishlist: [],
      recipeList: [],
      orderId: 0,
      orderValue: 0,
      userName: window.sessionStorage.getItem("login"),
    };
    this.handleValueChange = this.handleValueChange.bind(this);
    this.setOrderId = this.setOrderId.bind(this);
    this.setOrderValue = this.setOrderValue.bind(this);
  }
  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    axios
      .get("/api/dish", config)
      .then((res) => {
        this.setState({ dishlist: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
    axios
      .get("/api/recipe", config)
      .then((res) => {
        this.setState({ recipeList: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
  }

  handleValueChange() {
    this.setState({ value: !this.state.value });
    // console.log(this.state.value);
  }

  setOrderId(newOrderId) {
    this.state.orderId = newOrderId;
  }

  setOrderValue(totalOrderValue) {
    this.state.orderValue = totalOrderValue;
  }

  render() {
    if (!this.state.value) {
      return (
        <div>
          <Header />
          <DishListClient
            orderId={this.state.orderId}
            dishlist={this.state.dishlist}
            recipeList={this.state.recipeList}
            userName={this.state.userName}
            isOrderPlaced={this.handleValueChange}
            setOrderId={this.setOrderId}
            setOrderValue={this.setOrderValue}
          />
        </div>
      );
    }

    return (
      <div>
        <Header />
        <OrderSummary
          isOrderPlaced={this.handleValueChange}
          orderId={this.state.orderId}
          dishlist={this.state.dishlist}
          orderValue={this.state.orderValue}
        />
      </div>
    );
  }
}
