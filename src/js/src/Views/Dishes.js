import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Button, Row, Col } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

import DishAdd from "../components/DishAdd";
import RefreshToken from "../components/RefreshToken";

export default class Dishes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ingredientsList: [],
      rendering: false,
    };
    this.switchHandle = this.switchHandle.bind(this);
  }

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    axios
      .get("/api/ingredient", config)
      .then((res) => {
        this.setState({ ingredientsList: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
  }

  switchHandle() {
    this.setState({ rendering: !this.state.rendering });
  }
  async removeDish(dishId) {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    try {
      const response = await axios
        .delete("/api/dish/remove/" + dishId, config)
        .catch(() => {
          RefreshToken();
        });
      window.location.reload(false);
    } catch {
      RefreshToken();
    }
  }

  getIngredients(dishName) {
    let filtered = "";
    let ingredients = this.props.recipeList.filter(
      (recipe) => recipe.dishName === dishName
    );

    for (let i = 0; i < ingredients.length; i++) {
      filtered += ingredients[i].ingredientName + ", ";
    }
    filtered = filtered.substring(0, filtered.length - 2);
    return filtered;
  }

  renderRemovingButton(element, dishId) {
    if (window.sessionStorage.getItem("ROLE") === "ROLE_MANAGER") {
      if (element === "th") {
        return <th>Remove</th>;
      }
      if (element === "td") {
        return (
          <td>
            <Button
              variant="warning"
              onClick={() => {
                this.removeDish(dishId);
              }}
            >
              -
            </Button>
          </td>
        );
      }
    }
    return null;
  }

  renderDishesTable() {
    let dishes = this.props.dishlist;
    if (this.state.dishType) {
      dishes = [];
      for (let i = 0; i < this.props.dishlist.length; i++) {
        if (this.state.dishType == this.props.dishlist[i].dishType) {
          dishes.push(this.props.dishlist[i]);
        }
      }
    }
  }

  render() {
    if (!this.state.rendering) {
      return (
        <div>
          <Form>
            <Form.Check
              type="switch"
              id="custom-switch"
              label="Dishes list | Add or edit dish"
              onChange={this.switchHandle}
            />
          </Form>

          <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
            <thead>
              <tr>
                <th style={{ width: "5%" }}>#</th>
                <th style={{ width: "35%" }}>Dish name</th>
                <th style={{ width: "30%" }}>Ingredients</th>
                <th style={{ width: "30%" }}>Price</th>
                {this.renderRemovingButton("th", null)}
              </tr>
            </thead>
            <tbody>
              {
                (this.props.dishlist && this.props,
                this.props.dishlist
                  .sort((a, b) => (a.dishId > b.dishId ? 1 : -1))
                  .map((dish, i) => (
                    <tr key={i}>
                      <td className="align-middle">{dish.dishId}</td>
                      <td>{dish.dishName}</td>
                      <td>{this.getIngredients(dish.dishName)}</td>
                      <td>{dish.dishPrice}</td>
                      {this.renderRemovingButton("td", dish.dishId)}
                    </tr>
                  )))
              }
            </tbody>
          </Table>
        </div>
      );
    } else {
      return (
        <div>
          <Form>
            <Form.Check
              type="switch"
              id="custom-switch"
              label="Dishes list | Add or edit dish"
              onChange={this.switchHandle}
            />
          </Form>
          <DishAdd
            dishlist={this.props.dishlist}
            ingredientsList={this.state.ingredientsList}
          />
        </div>
      );
    }
  }
}
