import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import { Row, Col } from "react-bootstrap";
import axios from "axios";

import RecipesList from "../components/RecipesList";
import OrdersList from "../components/OrdersList";
import RefreshToken from "../components/RefreshToken";
import Header from "./Header";

export default class Kitchen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // value: false,
      rendering: false,
      dishlist: [],
      // recipeList: [],
      // usersList: [],

      // orderId: 0,
      // orderValue: 0,
      // userName: window.sessionStorage.getItem("login"),
    };
    this.switchHandle = this.switchHandle.bind(this);
  }

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    axios
      .get("api/dish", config)
      .then((res) => {
        this.setState({ dishlist: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
  }

  switchHandle() {
    this.setState({ rendering: !this.state.rendering });
  }

  render() {
    // if (sessionStorage.getItem("ROLE") !== "ROLE_KITCHEN") {
    //   console.error(403);
    //   return <div> Unauthorized! </div>;
    // }
    if (!this.state.rendering) {
      return (
        <div>
          <Row>
            <Col>
              <Header />
            </Col>
          </Row>
          <Row>
            <Col>
              <Form>
                <Form.Check
                  type="switch"
                  id="custom-switch"
                  label="Orders list | Recpies list"
                  onChange={this.switchHandle}
                />
              </Form>
            </Col>
          </Row>
          <OrdersList dishlist={this.state.dishlist} />
        </div>
      );
    } else {
      return (
        <div>
          <Row>
            <Col>
              <Header />
            </Col>
          </Row>
          <Row>
            <Col>
              <Form>
                <Form.Check
                  type="switch"
                  id="custom-switch"
                  label="Orders list | Recpies list"
                  onChange={this.switchHandle}
                />
              </Form>
            </Col>
          </Row>
          <RecipesList dishlist={this.state.dishlist} />
        </div>
      );
    }
  }
}
