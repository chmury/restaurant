import React, { Component } from "react";
import axios from "axios";
import Form from "react-bootstrap/Form";
import { Row, Col, Button } from "react-bootstrap";
import RefreshToken from "./RefreshToken";

class DishAdd extends Component {
  constructor() {
    super();
    this.state = {
      dishName: "",
      dishPrice: "",
      dishType: "starter",
      checkedIngredients: [],
      // ingredientlist: [],

      chosenDishName: "",
      editDishName: "",
      editDishPrice: "",
      editDishType: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.addNewDish = this.addNewDish.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.removeIngredientsFromList = this.removeIngredientsFromList.bind(this);
  }

  // componentDidMount() {
  //   const config = {
  //     headers: {
  //       Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
  //     },
  //     proxy: {
  //       host: "localhost",
  //       port: 8080,
  //     },
  //   };
  //   axios.get("/api/ingredient", config).then((res) => {
  //     this.setState({ ingredientlist: res.data });
  //   });
  // }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleCheckboxChange(event) {
    console.log("tak");
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    if (value) {
      this.setState({
        checkedIngredients: this.state.checkedIngredients.concat(name),
      });
    }
    if (!value) {
      this.removeIngredientsFromList(event);
    }
  }

  removeIngredientsFromList(event) {
    var array = [...this.state.checkedIngredients];
    var index = array.indexOf(event.target.name);
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({ checkedIngredients: array });
    }
  }

  async addRecipe(props) {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
        },
        proxy: {
          host: "localhost",
          port: 8080,
        },
      };
      const response = await axios
        .post(
          "/api/recipe",
          {
            dishName: this.state.dishName,
            ingredientName: props,
          },
          config
        )
        .catch(() => {
          RefreshToken();
        });

      console.log("Returned data:", response);
      window.location.reload(false);
    } catch (e) {
      console.log(`Axios request failed: ${e}`);
      RefreshToken();
    }
  }

  async addNewDish() {
    this.state.checkedIngredients &&
      this.state.checkedIngredients.map((checkedIngr, i) =>
        this.addRecipe(checkedIngr)
      );

    try {
      const config = {
        headers: {
          Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
        },
        proxy: {
          host: "localhost",
          port: 8080,
        },
      };
      const response = await axios
        .post(
          "/api/dish",
          {
            dishName: this.state.dishName,
            dishPrice: this.state.dishPrice,
            dishType: this.state.dishType,
          },
          config
        )
        .catch(() => {
          RefreshToken();
        });

      // console.log("Returned data:", response);
      window.location.reload(false);
    } catch (e) {
      console.log(`Axios request failed: ${e}`);
      RefreshToken();
    }
  }

  renderSelect() {
    // console.log(this.props.dishlist);
    const dishNameListString = [
      ...new Set(this.props.dishlist.map((res) => res.dishName)),
    ];
    // console.log(dishTypeString);
    let a = [];
    a.push(<option>{""}</option>);
    for (let i = 0; i < dishNameListString.length; i++) {
      a.push(<option>{dishNameListString[i]}</option>);
    }
    return a;
  }

  editDish() {
    if (
      this.state.chosenDishName ||
      this.state.editDishName ||
      this.state.editDishPrice ||
      this.state.editDishType
    ) {
      try {
        const config = {
          headers: {
            Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
          },
          proxy: {
            host: "localhost",
            port: 8080,
          },
        };
        axios
          .post(
            "/api/dish/update/" + this.state.chosenDishName,
            {
              dishName: this.state.editDishName,
              dishPrice: this.state.editDishPrice,
              dishType: this.state.editDishType,
            },
            config
          )
          .catch(() => {
            RefreshToken();
          });
        axios
          .post(
            "/api/recipe/update/" +
              this.state.editDishName +
              "/" +
              this.state.chosenDishName,
            {},
            config
          )
          .catch(() => {
            RefreshToken();
          });

        window.location.reload(false);
      } catch (e) {
        console.log(`Axios request failed: ${e}`);
      }
    } else {
      alert("All fields must be filled!");
    }

    // alert(this.state.chosenUserName);
  }

  render() {
    return (
      <Row>
        <Col
          xs={{ span: 6, offset: 3 }}
          md={{ span: 4, offset: 1 }}
          className="myCustomDishForm"
        >
          <h4>Add new dish</h4>
          <Form>
            <Form.Group controlId="formBasicDishAdd">
              <Form.Label>Dish name</Form.Label>
              <Form.Control
                type="text"
                name="dishName"
                placeholder="Enter dish name here"
                onChange={this.handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formBasicDishPrice">
              <Form.Label>Dish price</Form.Label>
              <Form.Control
                type="number"
                name="dishPrice"
                placeholder="Add dish price"
                onChange={this.handleChange}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Choose dish type</Form.Label>
              <Form.Control
                as="select"
                name="dishType"
                onChange={this.handleChange}
                custom
              >
                <option value=""></option>
                <option value="starter">starter</option>
                <option value="pizza">pizza</option>
                <option value="pasta">pasta</option>
                <option value="sides">sides</option>
                <option value="beverages">beverages</option>
                <option value="burgers">burgers</option>
                <option value="desserts">desserts</option>
              </Form.Control>
            </Form.Group>

            {/* {this.state.ingredientlist &&
              this.state.ingredientlist.map((ingredient, i) => (
                <ul key={i}>
                  <input
                    type="checkbox"
                    name={ingredient.ingredientName}
                    onChange={this.handleCheckboxChange}
                  />
                  {ingredient.ingredientName}
                </ul>
              ))} */}

            <Form>
              <Form.Label>Choose ingredients</Form.Label>
              <div className="mb-3">
                {/* <div key={`inline-ingredient`} className="mb-3"> */}
                {this.props.ingredientsList &&
                  this.props.ingredientsList.map((ingredient, i) => (
                    <Form.Check
                      inline
                      name={ingredient.ingredientName}
                      label={ingredient.ingredientName}
                      type="checkbox"
                      id={i + 1}
                      onChange={this.handleCheckboxChange}
                    />
                  ))}
              </div>
              {/* </div> */}
            </Form>

            <Button
              variant="primary"
              block
              type="submit"
              onClick={() => {
                this.addNewDish();
              }}
            >
              Create
            </Button>
          </Form>
        </Col>

        <Col
          xs={{ span: 6, offset: 3 }}
          md={{ span: 4, offset: 1 }}
          className="myCustomEditDishForm"
        >
          <h4>Edit existing dish</h4>
          <Form>
            <Form.Group>
              <Form.Label>Choose dish to edit</Form.Label>
              <Form.Control
                as="select"
                name="chosenDishName"
                onChange={this.handleChange}
                custom
              >
                {this.renderSelect()}
              </Form.Control>
            </Form.Group>

            <Form.Group>
              <Form.Label>New dish name</Form.Label>
              <Form.Control
                type="text"
                name="editDishName"
                placeholder="Enter new dish name"
                onChange={this.handleChange}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>New dish price</Form.Label>
              <Form.Control
                type="number"
                name="editDishPrice"
                placeholder="Add new dish price"
                onChange={this.handleChange}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Choose new dish type</Form.Label>
              <Form.Control
                as="select"
                name="editDishType"
                onChange={this.handleChange}
                custom
              >
                <option value=""></option>
                <option value="starter">starter</option>
                <option value="pizza">pizza</option>
                <option value="pasta">pasta</option>
                <option value="sides">sides</option>
                <option value="beverages">beverages</option>
                <option value="burgers">burgers</option>
                <option value="desserts">desserts</option>
              </Form.Control>
            </Form.Group>

            <Button
              variant="info"
              block
              type="submit"
              onClick={() => {
                this.editDish();
              }}
            >
              Edit
            </Button>
          </Form>
        </Col>
      </Row>
    );
  }
}

export default DishAdd;

/*
  render() {
    return (
      <form>
        <h4>Add Dish Form</h4>

        <br />
        <input
          type="text"
          //value={this.state.ingredientName}
          name="dishName"
          placeholder="Dish Name"
          onChange={this.handleChange}
        />
        <br />
        <input
          type="text"
          //value={this.state.ingredientName}
          name="dishPrice"
          placeholder="Dish Price"
          onChange={this.handleChange}
        />
        <br />
        <select name="dishType" onChange={this.handleChange}>
          <option value="starter">starter</option>
          <option value="pizza">pizza</option>
          <option value="pasta">pasta</option>
          <option value="sides">sides</option>
          <option value="beverages">beverages</option>
          <option value="burgers">burgers</option>
          <option value="desserts">desserts</option>
        </select>
        <div>
          {this.state.ingredientlist &&
            this.state.ingredientlist.map((ingredient, i) => (
              <ul key={i}>
                <input
                  type="checkbox"
                  name={ingredient.ingredientName}
                  onChange={this.handleCheckboxChange}
                />
                {ingredient.ingredientName}
              </ul>
            ))}
        </div>

        <input type="button" value="Add Dish" onClick={this.handleClick} />
      </form>
    );
  }
}

*/
