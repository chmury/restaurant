import React from "react";
import axios from "axios";
import RefreshToken from "./RefreshToken";
class TableOfDish extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dishlist: [],
    };
  }

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };

    axios
      .get("http://localhost:8080/api/dish", config)
      .then((res) => {
        this.setState({ dishlist: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
  }

  //TO-DO Delete nie działa, zły warunek axios
  async deleteDish(dish) {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    const response = await axios
      .delete("api/dish/" + dish.dishId, config)
      .catch(() => {
        RefreshToken();
      });
    console.log(response.status);
    window.location.reload(false);

    response = await axios
      .delete("api/recipe/dish?dishName=" + dish.dishName, config)
      .catch(() => {
        RefreshToken();
      });
    console.log(response.status);
    window.location.reload(false);
  }

  renderHeaderTable() {
    return (
      <thead>
        <tr>
          <th>ID</th>
          <th>NAME</th>
          <th>PRICE</th>
          <th>DELETE ?</th>
        </tr>
      </thead>
    );
  }
  renderBodyTable() {
    return (
      <tbody>
        {this.state.dishlist &&
          this.state.dishlist.map((dish, i) => (
            <tr key={i}>
              <td>{dish.dishId}</td>
              <td>{dish.dishName}</td>
              <td>{dish.dishPrice}</td>
              <td>
                <input
                  type="button"
                  value="Delete"
                  onClick={() => this.deleteDish(dish)}
                />
              </td>
            </tr>
          ))}
      </tbody>
    );
  }

  render() {
    return (
      <div>
        <h4>Dish List</h4>
        <table>
          {this.renderHeaderTable()}
          {this.renderBodyTable()}
        </table>
      </div>
    );
  }
}

export default TableOfDish;
