import React from "react";
import axios from "axios";
export default function RefreshToken() {
  axios
    .post(
      "/oauth/token?grant_type=refresh_token&refresh_token=" +
        window.sessionStorage.getItem("jwt_refresh"),
      null,
      {
        auth: {
          username: "admin",
          password: "admin",
        },
        proxy: {
          host: "localhost",
          port: 8080,
        },
      }
    )
    .then((res) => {
      window.sessionStorage.removeItem("jwt");
      window.sessionStorage.removeItem("jwt_refresh");
      window.sessionStorage.setItem("jwt", res.data.access_token);
      window.sessionStorage.setItem("jwt_refresh", res.data.refresh_token);
      window.location.reload(false);
    })
    .catch((e) => {
      console.log(e);

      // console.log("401 Unauthorized");
    });
}
