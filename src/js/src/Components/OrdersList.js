import React, { Component } from "react";
import axios from "axios";
import RefreshToken from "./RefreshToken";

import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import { Form } from "react-bootstrap";

export default class OrdersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ordersList: [],
      archiveOrdersList: [],
      renderingOrderSList: false,
    };
    this.changeOrderStatus = this.changeOrderStatus.bind(this);
    this.switchOrderHandle = this.switchOrderHandle.bind(this);
  }

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    axios
      .get("/api/order/all", config)
      .then((res) => {
        this.setState({ ordersList: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
    axios
      .get("/api/order/allArchive", config)
      .then((res) => {
        this.setState({ archiveOrdersList: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
  }

  switchOrderHandle() {
    this.setState({ renderingOrderSList: !this.state.rendering });
  }

  findDishesNames(order) {
    let arrayDishes = order.orderedDishes;
    let arrayD = this.props.dishlist;
    let finded = "";
    for (let i = 0; i < arrayDishes.length; i++) {
      for (let j = 0; j < arrayD.length; j++) {
        if (arrayDishes[i] === arrayD[j].dishId) {
          finded += arrayD[j].dishName + ", ";
        }
      }
    }
    finded = finded.substr(0, finded.length - 2);
    return finded;
  }

  calculateOrderAmount(order) {
    let arrayDishes = order.orderedDishes;
    let arrayD = this.props.dishlist;
    let amount = 0;
    for (let i = 0; i < arrayDishes.length; i++) {
      for (let j = 0; j < arrayD.length; j++) {
        if (arrayDishes[i] === arrayD[j].dishId) {
          amount += arrayD[j].dishPrice;
        }
      }
    }
    return amount;
  }

  changeOrderStatus(id, newStatus) {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
        },
        proxy: {
          host: "localhost",
          port: 8080,
        },
      };

      const response = axios
        .post("/api/order/status/" + id + "/" + newStatus, {}, config)
        .catch(() => {
          RefreshToken();
        });
      window.location.reload(false);
    } catch (e) {
      console.log(`Axios request failed: ${e}`);
      RefreshToken();
    }
  }

  renderMaintananceButton(order) {
    if (
      window.sessionStorage.getItem("ROLE") === "ROLE_WAITER" ||
      window.sessionStorage.getItem("ROLE") === "ROLE_MANAGER"
    ) {
      if (order.orderStatus === "Wydane") {
        return (
          <Button
            variant="warning"
            onClick={() => {
              this.changeOrderStatus(order.orderId, "Oplacone");
            }}
          >
            Opłać
          </Button>
        );
      }
      if (order.orderStatus === "Gotowe") {
        return (
          <Button
            variant="warning"
            onClick={() => {
              this.changeOrderStatus(order.orderId, "Wydane");
            }}
          >
            Wydaj
          </Button>
        );
      }
    }

    if (
      window.sessionStorage.getItem("ROLE") === "ROLE_KITCHEN" ||
      window.sessionStorage.getItem("ROLE") === "ROLE_MANAGER"
    ) {
      if (order.orderStatus === "Zamówione") {
        return (
          <Button
            variant="warning"
            onClick={() => {
              this.changeOrderStatus(order.orderId, "Gotowe");
            }}
          >
            Gotowe
          </Button>
        );
      }
    }

    return (
      <Button variant="warning" disabled>
        Niedostępne
      </Button>
    );
  }
  renderSwitch() {
    if (window.sessionStorage.getItem("ROLE") == "ROLE_MANAGER") {
      return (
        <Form>
          <Form.Check
            type="switch"
            id="custom-switch"
            label="Currents order | Archive orders"
            onChange={this.switchOrderHandle}
          />
        </Form>
      );
    }
  }

  render() {
    if (!this.state.renderingOrderSList) {
      return (
        <div>
          {this.renderSwitch()}
          <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
            <thead>
              <tr>
                <th style={{ width: "5%" }}>#</th>
                <th style={{ width: "35%" }}>Dish name</th>
                <th style={{ width: "30%" }}>Customer</th>
                <th style={{ width: "10%" }}>Price [PLN]</th>
                <th style={{ width: "10%" }}>Status</th>
                <th style={{ width: "10%" }}>Maintain</th>
              </tr>
            </thead>
            <tbody>
              {this.state.ordersList &&
                this.state.ordersList
                  .sort((a, b) => (a.orderId > b.orderId ? 1 : -1))
                  .map((order, i) => (
                    <tr key={i}>
                      <td className="align-middle">{order.orderId}</td>
                      <td>{this.findDishesNames(order)}</td>
                      <td>{order.userName}</td>
                      <td className="align-middle">
                        {this.calculateOrderAmount(order)}
                      </td>
                      <td>{order.orderStatus}</td>
                      <td className="align-middle">
                        {this.renderMaintananceButton(order)}
                      </td>
                    </tr>
                  ))}
            </tbody>
          </Table>
        </div>
      );
    } else {
      return (
        <div>
          {this.renderSwitch()}
          <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
            <thead>
              <tr>
                <th style={{ width: "5%" }}>#</th>
                <th style={{ width: "35%" }}>Dish name</th>
                <th style={{ width: "30%" }}>Customer</th>
                <th style={{ width: "10%" }}>Price [PLN]</th>
                <th style={{ width: "10%" }}>Status</th>
              </tr>
            </thead>
            <tbody>
              {this.state.archiveOrdersList &&
                this.state.archiveOrdersList
                  .sort((a, b) => (a.orderId < b.orderId ? 1 : -1))
                  .map((order, i) => (
                    <tr key={i}>
                      <td className="align-middle">{order.orderId}</td>
                      <td>{this.findDishesNames(order)}</td>
                      <td>{order.userName}</td>
                      <td className="align-middle">
                        {this.calculateOrderAmount(order)}
                      </td>
                      <td>{order.orderStatus}</td>
                    </tr>
                  ))}
            </tbody>
          </Table>
        </div>
      );
    }
  }
}
