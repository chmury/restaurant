import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Button } from "react-bootstrap";
import axios from "axios";
import Form from "react-bootstrap/Form";

import UserAdd from "../components/UserAdd";
import RefreshToken from "../components/RefreshToken";

export default class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rendering: false,
    };
    this.switchHandle = this.switchHandle.bind(this);
  }

  async removeUser(userName) {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    const response = await axios
      .delete("/api/user/" + userName, config)
      .catch(() => {
        RefreshToken();
      });

    window.location.reload(false);
  }

  switchHandle() {
    this.setState({ rendering: !this.state.rendering });
  }

  render() {
    if (!this.state.rendering) {
      return (
        <div>
          <Form>
            <Form.Check
              type="switch"
              id="custom-switch"
              label="Users list | Add or edit user"
              onChange={this.switchHandle}
            />
          </Form>

          <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
            <thead>
              <tr>
                <th style={{ width: "5%" }}>#</th>
                <th rowSpan="2" style={{ width: "65%" }}>
                  User name
                </th>
                <th style={{ width: "20%" }}>User role</th>
                <th style={{ width: "10%" }}>Remove</th>
              </tr>
            </thead>

            <tbody>
              {this.props.usersList &&
                this.props.usersList.map((user, i) => (
                  <tr key={i}>
                    {/* <td>{dish.dishId}</td> */}
                    <td>{i + 1}</td>
                    <td>{user.userName}</td>
                    <td>
                      {user.authority.substring(5, user.authority.length)}
                    </td>
                    <td>
                      <Button
                        variant="warning"
                        onClick={() => this.removeUser(user.userName)}
                      >
                        -
                      </Button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </div>
      );
    } else {
      return (
        <div>
          <Form>
            <Form.Check
              type="switch"
              id="custom-switch"
              label="Users list | Add or edit user"
              onChange={this.switchHandle}
            />
          </Form>
          <UserAdd usersList={this.props.usersList} />
        </div>
      );
    }
  }
}
