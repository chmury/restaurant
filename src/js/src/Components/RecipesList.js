import React from "react";
import axios from "axios";
import Table from "react-bootstrap/Table";
import { Row, Col, Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import RefreshToken from "./RefreshToken";

class RecipeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipeList: [],
      selectedDish: "",
      dishType: "",
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    axios
      .get("/api/recipe", config)
      .then((res) => {
        this.setState({ recipeList: res.data });
      })
      .catch(() => {
        RefreshToken();
      });
  }

  async removeRecipe(dishName) {
    const config = {
      headers: {
        Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
      },
      proxy: {
        host: "localhost",
        port: 8080,
      },
    };
    const response = await axios
      .delete("api/recipe/dish/" + dishName, config)
      .catch(() => {
        RefreshToken();
      });
    console.log(response.status);
    // window.location.reload(false);
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  renderSelect() {
    // console.log(this.props.dishlist);
    const dishTypeString = [
      ...new Set(this.props.dishlist.map((res) => res.dishType)),
    ];
    // console.log(dishTypeString);
    let a = [];
    a.push(<option>{""}</option>);
    for (let i = 0; i < dishTypeString.length; i++) {
      a.push(<option>{dishTypeString[i]}</option>);
    }
    return a;
  }

  selectDishType() {
    return (
      <Row>
        <Col xs={{ span: 3 }}>
          <Form>
            <Form.Group controlId="exampleForm.SelectCustom">
              <Form.Label>Filter dishes by type</Form.Label>
              <Form.Control
                name="dishType"
                as="select"
                custom
                onChange={this.handleChange}
              >
                {this.renderSelect()}
              </Form.Control>
            </Form.Group>
          </Form>
        </Col>
      </Row>
    );
  }

  getIngredients(dishName) {
    let filtered = "";
    let ingredients = this.state.recipeList.filter(
      (recipe) => recipe.dishName === dishName
    );

    for (let i = 0; i < ingredients.length; i++) {
      filtered += ingredients[i].ingredientName + ", ";
    }
    filtered = filtered.substring(0, filtered.length - 2);
    return filtered;
  }

  renderRemovingButton(element, dishName) {
    console.log("tak");
    if (window.sessionStorage.getItem("ROLE") === "ROLE_MANAGER") {
      if (element === "th") {
        return <th>Remove</th>;
      }
      if (element === "td") {
        return (
          <td>
            <Button
              variant="warning"
              onClick={() => {
                this.removeRecipe(dishName);
              }}
            >
              -
            </Button>
          </td>
        );
      }
    }
    return null;
  }

  renderDishesTable() {
    let dishes = this.props.dishlist;
    if (this.state.dishType) {
      dishes = [];
      for (let i = 0; i < this.props.dishlist.length; i++) {
        if (this.state.dishType == this.props.dishlist[i].dishType) {
          dishes.push(this.props.dishlist[i]);
        }
      }
    }

    return (
      <div>
        <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
          <thead>
            <tr>
              <th style={{ width: "5%" }}>#</th>
              <th style={{ width: "35%" }}>Dish name</th>
              <th style={{ width: "60%" }}>Ingredients</th>
              {this.renderRemovingButton("th", null)}
            </tr>
          </thead>
          <tbody>
            {dishes &&
              dishes
                .sort((a, b) => (a.dishId > b.dishId ? 1 : -1))
                .map((dish, i) => (
                  <tr key={i}>
                    <td className="align-middle">{dish.dishId}</td>
                    <td>{dish.dishName}</td>
                    <td>{this.getIngredients(dish.dishName)}</td>
                    {this.renderRemovingButton("td", dish.dishName)}
                  </tr>
                ))}
          </tbody>
        </Table>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.selectDishType()}
        {this.renderDishesTable()}
      </div>
    );
  }
}

export default RecipeList;
