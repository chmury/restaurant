import React, { Component } from "react";
import axios from "axios";
import Form from "react-bootstrap/Form";
import { Row, Col, Button } from "react-bootstrap";
import RefreshToken from "./RefreshToken";
import "../css/form.css";

class UserAdd extends Component {
  constructor() {
    super();
    this.state = {
      userName: "",
      userPassword: "",
      userRole: "",

      chosenUserName: "",
      editUserPassword: "",
      editUserRole: "",
      // roleForChosen: "elo",
    };

    this.handleChange = this.handleChange.bind(this);
    // this.getUserRole = this.getUserRole.bind(this);
  }

  // componentDidMount() {
  //   const config = {
  //     headers: {
  //       Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
  //     },
  //     proxy: {
  //       host: "localhost",
  //       port: 8080,
  //     },
  //   };
  //   axios.get("api/ingredient", config).then((res) => {
  //     this.setState({ ingredientlist: res.data });
  //   });
  // }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
    // this.getUserRole();
  }

  addNewUser() {
    if (this.state.userRole || this.state.userName || this.state.userPassword) {
      try {
        const config = {
          headers: {
            Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
          },
          proxy: {
            host: "localhost",
            port: 8080,
          },
        };
        axios
          .post(
            "/api/user",
            {
              userName: this.state.userName,
              password: this.state.userPassword,
              authority: this.state.userRole,
            },
            config
          )
          .catch(() => {
            RefreshToken();
          });

        window.location.reload(false);
      } catch (e) {
        console.log(`Axios request failed: ${e}`);
        RefreshToken();
      }
    } else {
      alert("All fields must be filled!");
    }
  }

  renderSelect() {
    // console.log(this.props.dishlist);
    const userNameListString = [
      ...new Set(this.props.usersList.map((res) => res.userName)),
    ];
    // console.log(dishTypeString);
    let a = [];
    a.push(<option>{""}</option>);
    for (let i = 0; i < userNameListString.length; i++) {
      a.push(<option>{userNameListString[i]}</option>);
    }
    return a;
  }

  // getUserRole() {
  //   let users = this.props.usersList;
  //   let role = "";
  //   for (let i = 0; i < users.length; i++) {
  //     if (users[i].userName === this.state.chosenUserName) {
  //       role = users[i].authority;
  //     }
  //   }
  //   // this.setState({ roleForChosen: role });
  // }

  editUser() {
    if (
      this.state.chosenUserName ||
      this.state.editUserPassword ||
      this.state.editUserRole
    ) {
      try {
        const config = {
          headers: {
            Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
          },
          proxy: {
            host: "localhost",
            port: 8080,
          },
        };
        axios
          .post(
            "/api/user/update",
            {
              userName: this.state.chosenUserName,
              password: this.state.editUserPassword,
              authority: this.state.editUserRole,
            },
            config
          )
          .catch(() => {
            RefreshToken();
          });

        window.location.reload(false);
      } catch (e) {
        console.log(`Axios request failed: ${e}`);
        RefreshToken();
      }
    } else {
      alert("All fields must be filled!");
    }

    // alert(this.state.chosenUserName);
  }

  render() {
    return (
      <Row>
        <Col
          xs={{ span: 6, offset: 3 }}
          md={{ span: 4, offset: 1 }}
          className="myCustomForm"
        >
          <h4>Add new user</h4>
          <Form>
            <Form.Group controlId="formBasicLogin">
              <Form.Label>User name</Form.Label>
              <Form.Control
                type="login"
                name="userName"
                placeholder="Enter user name here"
                onChange={this.handleChange}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                name="userPassword"
                placeholder="Create password for user"
                onChange={this.handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Choose role for user</Form.Label>
              <Form.Control
                as="select"
                name="userRole"
                onChange={this.handleChange}
                custom
              >
                <option value=""></option>
                <option value="ROLE_CUSTOMER">Customer</option>
                <option value="ROLE_WAITER">Waiter</option>
                <option value="ROLE_KITCHEN">Kitchen</option>
                <option value="ROLE_MANAGER">Manager</option>
              </Form.Control>
            </Form.Group>

            <Button
              variant="primary"
              block
              type="submit"
              onClick={() => {
                this.addNewUser();
              }}
            >
              Create
            </Button>
          </Form>
        </Col>
        <Col
          xs={{ span: 6, offset: 3 }}
          md={{ span: 4, offset: 1 }}
          className="myCustomForm"
        >
          <h4>Edit existing user</h4>
          <Form>
            <Form.Group>
              <Form.Label>Choose user to edit</Form.Label>
              <Form.Control
                as="select"
                name="chosenUserName"
                onChange={this.handleChange}
                custom
              >
                {this.renderSelect()}
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="formNewPassword">
              <Form.Label>New password</Form.Label>
              <Form.Control
                type="password"
                name="editUserPassword"
                placeholder="Create new password for user"
                onChange={this.handleChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Choose new role for user</Form.Label>
              <Form.Control
                as="select"
                name="editUserRole"
                onChange={this.handleChange}
                custom
              >
                <option value=""></option>
                <option value="ROLE_CUSTOMER">Customer</option>
                <option value="ROLE_WAITER">Waiter</option>
                <option value="ROLE_KITCHEN">Kitchen</option>
                <option value="ROLE_MANAGER">Manager</option>
              </Form.Control>
            </Form.Group>

            <Button
              variant="info"
              block
              type="submit"
              onClick={() => {
                this.editUser();
              }}
            >
              Edit
            </Button>
          </Form>
        </Col>
      </Row>

      // <form>
      //   <h4>Add Dish Form</h4>
      //   <input
      //     type="text"
      //     // value={this.state.ingredientId}
      //     name="dishId"
      //     placeholder="Dish ID"
      //     onChange={this.handleChange}
      //   />
      //   <br />
      //   <input
      //     type="text"
      //     //value={this.state.ingredientName}
      //     name="dishName"
      //     placeholder="Dish Name"
      //     onChange={this.handleChange}
      //   />
      //   <br />
      //   <input
      //     type="text"
      //     //value={this.state.ingredientName}
      //     name="dishPrice"
      //     placeholder="Dish Price"
      //     onChange={this.handleChange}
      //   />
      //   <br />
      //   <select name="dishType" onChange={this.handleChange}>
      //     <option value="starter">starter</option>
      //     <option value="pizza">pizza</option>
      //     <option value="pasta">pasta</option>
      //     <option value="sides">sides</option>
      //     <option value="beverages">beverages</option>
      //     <option value="burgers">burgers</option>
      //     <option value="desserts">desserts</option>
      //   </select>
      //   <div>
      //     {this.state.ingredientlist &&
      //       this.state.ingredientlist.map((ingredient, i) => (
      //         <ul key={i}>
      //           <input
      //             type="checkbox"
      //             name={ingredient.ingredientName}
      //             onChange={this.handleCheckboxChange}
      //           />
      //           {ingredient.ingredientName}
      //         </ul>
      //       ))}
      //   </div>

      //   <input type="button" value="Add Dish" onClick={this.handleClick} />
      // </form>
    );
  }
}
export default UserAdd;
