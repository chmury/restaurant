import React from "react";
import axios from "axios";
import { Row, Col, Button } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import RefreshToken from "./RefreshToken";

// TODO dopisać pobieranie składników i wyświetlanie ich w tabeli

class DishListClient extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderedDishList: [],
      orderValue: 0,
      ingredientsList: "",
    };
    this.orderDish = this.orderDish.bind(this);
    this.placeOrder = this.placeOrder.bind(this);
  }

  orderDish(orderedDish) {
    this.setState({
      orderedDishList: this.state.orderedDishList.concat(orderedDish),
      orderValue: this.state.orderValue + orderedDish.dishPrice,
    });
  }

  getIngredients(dishName) {
    let filtered = "";
    let ingredients = this.props.recipeList.filter(
      (recipe) => recipe.dishName === dishName
    );

    for (let i = 0; i < ingredients.length; i++) {
      filtered += ingredients[i].ingredientName + " ";
    }

    return filtered;
  }

  placeOrder() {
    let ordered = [];

    this.state.orderedDishList.forEach((element) => {
      ordered.push(element.dishId);
    });

    try {
      const config = {
        headers: {
          Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
        },
        proxy: {
          host: "localhost",
          port: 8080,
        },
      };

      axios
        .post(
          // const response = axios.post(
          "/api/order",
          {
            // TODO - inny orderID
            // orderId: 51,er
            userName: this.props.userName,
            orderedDishes: ordered,
            orderValue: this.state.orderValue,
          },
          config
        )
        .then((res) => {
          this.orderedDishList = [];
          this.props.setOrderId(res.data);
          // przekazanie wartosci orderValue do komponentu customer
          this.props.setOrderValue(this.state.orderValue);
          // ustawienie wartosci true / false w componencie customer (renderewanie menu / podsumowania zamowienia)

          this.props.isOrderPlaced();
        })
        .catch(() => {
          RefreshToken();
        });
    } catch (e) {
      console.log(`Axios request failed: ${e}`);
    }
  }

  removeDish(dishPrice, i) {
    this.state.orderedDishList.splice(i, 1);
    // console.log("dish: " + dishPrice);
    // console.log("dish: " + dish.dishPrice);
    this.setState({
      orderedDishList: this.state.orderedDishList,
      orderValue: this.state.orderValue - dishPrice,
    });
  }

  renderOrderingTable() {
    return (
      <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
        <thead>
          <tr>
            <th style={{ width: "5%" }}>#</th>
            <th rowSpan="2" style={{ width: "65%" }}>
              Dish name
            </th>
            <th style={{ width: "20%" }}>Price [PLN]</th>
            <th style={{ width: "10%" }}>Add in</th>
          </tr>
        </thead>
        <tbody>
          {this.props.dishlist &&
            this.props.dishlist.map((dish, i) => (
              <tr key={i}>
                <td className="align-middle">{dish.dishId}</td>
                <td>
                  {dish.dishName}
                  <br />
                  {this.getIngredients(dish.dishName)}
                </td>
                <td className="align-middle">{dish.dishPrice}</td>

                <td className="align-middle">
                  <Button
                    variant="warning"
                    onClick={() => this.orderDish(dish)}
                  >
                    +
                  </Button>
                </td>
              </tr>
            ))}
        </tbody>
      </Table>
    );
  }

  renderOrderSummaryTable() {
    return (
      <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
        <thead>
          <tr>
            <th style={{ width: "5%" }}>#</th>
            <th rowSpan="2" style={{ width: "65%" }}>
              Dish name
            </th>
            <th style={{ width: "20%" }}>Price [PLN]</th>
            <th style={{ width: "10%" }}>Remove</th>
          </tr>
        </thead>

        <tbody>
          {this.state.orderedDishList &&
            this.state.orderedDishList.map((dish, i) => (
              <tr key={i}>
                {/* <td>{dish.dishId}</td> */}
                <td>{i + 1}</td>
                <td>{dish.dishName}</td>
                <td>{dish.dishPrice}</td>
                <td>
                  <Button
                    variant="warning"
                    onClick={() => this.removeDish(dish.dishPrice, i)}
                  >
                    -
                  </Button>
                </td>
              </tr>
            ))}
          <tr>
            <td colSpan="2" style={{ textAlign: "right" }}>
              Summary order value:
            </td>
            <td colSpan="2">
              <b>{this.state.orderValue}</b>
            </td>
          </tr>
        </tbody>
      </Table>
    );
  }

  renderOrderSummary() {
    return (
      <Table bordered hover variant="dark" style={{ textAlign: "center" }}>
        <thead>
          <tr>
            <th style={{ width: "5%" }}>#</th>
            <th rowSpan="2" style={{ width: "65%" }}>
              Dish name
            </th>
            <th style={{ width: "20%" }}>Price [PLN]</th>
            <th style={{ width: "10%" }}>Remove</th>
          </tr>
        </thead>

        <tbody>
          {this.state.orderedDishList &&
            this.state.orderedDishList.map((dish, i) => (
              <tr key={i}>
                {/* <td>{dish.dishId}</td> */}
                <td>{i + 1}</td>
                <td>{dish.dishName}</td>
                <td>{dish.dishPrice}</td>
                <td>
                  <Button
                    variant="warning"
                    onClick={() => this.removeDish(dish.dishPrice, i)}
                  >
                    -
                  </Button>
                </td>
              </tr>
            ))}
        </tbody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        <h1>MENU</h1>
        {this.renderOrderingTable()}
        <Row>
          <Col xs={{ span: 6, offset: 3 }} sm={{ span: 4, offset: 4 }}>
            Order Summary
          </Col>
        </Row>
        {this.renderOrderSummaryTable()}
        <Row>
          <Col xs={{ span: 6, offset: 3 }} sm={{ span: 4, offset: 4 }}>
            <Button block variant="primary" onClick={this.placeOrder}>
              Place an order
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DishListClient;
