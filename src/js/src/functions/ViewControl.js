import history from "../History";

export default function ViewControl() {
  let userRole = window.sessionStorage.getItem("ROLE");
  if (userRole === "ROLE_MANAGER") {
    history.push("/Manager");
    window.location.reload();
  }
  if (userRole === "ROLE_WAITER") {
    history.push("/Waiter");
    window.location.reload();
  }
  if (userRole === "ROLE_KITCHEN") {
    history.push("/Kitchen");
    window.location.reload();
  }
  if (userRole === "ROLE_CUSTOMER") {
    history.push("/Menu");
    window.location.reload();
  }
}
