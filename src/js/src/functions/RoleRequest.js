import React from "react";
import axios from "axios";

export default function RoleRequest() {
  const config = {
    headers: {
      Authorization: `Bearer ` + window.sessionStorage.getItem("jwt"),
    },
  };
  const userName = window.sessionStorage.getItem("login");
  axios
    .get("http://localhost:8080/api/user/role/" + userName, config)
    .then((res) => window.sessionStorage.setItem("ROLE", res.data.userRole));
}
