package com.marste.restaurant.Recipe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

/**
 * @author Patryk Markowski
 */
@RequestMapping("api/recipe")
@RestController
@CrossOrigin("*")
public class RecipeController {

    private final RecipeService recipeService;
    @Autowired

    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }
    @GetMapping
    public List<Recipe> getAllRecipes()
    {
        return recipeService.selectAllRecipes();
    }
    @GetMapping("/dishIngredients")
    public  List<Recipe> getRecipesByDishName (String dishName)
    {
        return recipeService.selectRecipesByDishName(dishName);
    }
    @PostMapping
    public void addRecipe (@RequestBody @Valid Recipe recipe)
    {

        recipeService.addRecipe(recipe);
    }
    @DeleteMapping("/dish/{dishName}")
    public void deleteDishFromRecipes(@PathVariable String dishName)
    {
        recipeService.deleteDishFromRecipes(dishName);
    }
    @DeleteMapping("/ingredient")
    public void deleteIngredientFromDishRecipe (String dishName , String ingredientName)
    {
        recipeService.deleteIngredientFromDishRecipe(dishName,ingredientName);
    }
    @PostMapping(path = "/update/{dishName}/{oldDishName}")
    public void updateRecipe(@PathVariable String dishName,@PathVariable String oldDishName){
        recipeService.updateRecipe(dishName,oldDishName);
    }

}
