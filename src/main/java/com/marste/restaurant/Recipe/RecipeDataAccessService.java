package com.marste.restaurant.Recipe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Patryk Markowski
 */
@Repository
public class RecipeDataAccessService {

    private final JdbcTemplate jdbcTemplate;
    @Autowired

    public RecipeDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    int insertRecipe(Recipe recipe)
    {
        String sql = "" +
                "INSERT INTO recipe (" +
                " dishName, " +
                " ingredientName)"
                + "VALUES (?, ?)";
        return jdbcTemplate.update(
                sql,
                recipe.getDishName(),
                recipe.getIngredientName());
    }

    private RowMapper<Recipe> mapRecipeFomDb() {
        return (resultSet, i) -> {
            String dishName = resultSet.getString("dishName");
            String ingredientName = resultSet.getString("ingredientName");
            return new Recipe(
                    dishName,
                    ingredientName
            );
        };
    }

    List<Recipe> selectAllRecipes() {
        String sql = "" +
                "SELECT " +
                " dishName, " +
                " ingredientName " +
                "FROM recipe";


        return jdbcTemplate.query(sql, mapRecipeFomDb());
    }

    List<Recipe> selectRecipeByDishName (String dishName)
    {
        String sql = "" +
                "SELECT " +
                " dishName, " +
                " ingredientName " +
                "FROM recipe " +
                "WHERE dishName = '"+dishName+"'";
        return jdbcTemplate.query(sql, mapRecipeFomDb());
    }
    public int deleteDishFromRecipes(String dishName)
    {
        String sql = "" +
                "DELETE FROM recipe " +
                "WHERE dishName = '"+dishName+"'";

        return jdbcTemplate.update(sql);
    }
    public int deleteIngredientFromDishRecipe(String dishName,String ingredientName)
    {
        String sql = "" +
                "DELETE FROM recipe " +
                "WHERE dishName = '"+dishName+"' AND ingredientName ='"+ingredientName+"'";

        return jdbcTemplate.update(sql);
    }

    public void updateRecipe(String dishName, String oldDishName) {
        String sql = ""+
                "UPDATE recipe "+
                "SET dishName = ? " +
                " WHERE dishName = ?";
        jdbcTemplate.update(sql,dishName,oldDishName);
    }

   /* List<String> selectAllIngredientsByDishNameString(String dishName)
    {

    }
*/

    /*

        Dish selectDishById(int id)
        {
            String sql = "" +
                    "SELECT " +
                    " dishId, " +
                    " dishName, " +
                    " dishPrice, " +
                    " dishType " +
                    "FROM dish " +
                    "WHERE dishId ="+id;

            return jdbcTemplate.queryForObject(sql,mapDishFomDb());
        }
        Dish selectDishByName(String dishName)
        {
            String sql = "" +
                    "SELECT " +
                    " dishId, " +
                    " dishName, " +
                    " dishPrice, " +
                    " dishType " +
                    "FROM dish " +
                    "WHERE dishName ='"+dishName+"'";

            return jdbcTemplate.queryForObject(sql,mapDishFomDb());
        }

        int deleteDishById(int dishId) {
            String sql = "" +
                    "DELETE FROM dish " +
                    "WHERE dishId = ?";
            return jdbcTemplate.update(sql, dishId);
        }
        List<Dish> selectDishbyType(String dishType) {
            //TODO dopisać warunek żeby dishType był jednym z typów
            String sql = "" +
                    "SELECT " +
                    " dishId, " +
                    " dishName, " +
                    " dishPrice, " +
                    " dishType " +
                    "FROM dish " +
                    "WHERE dishType = '" + dishType+"'";

            return jdbcTemplate.query(sql, mapDishFomDb());
        }

        int updateDishName(int dishId , String dishName)
        {
            String sql = "" +
                    "UPDATE dish " +
                    "SET dishName = ? " +
                    "WHERE dishId = ?";
            return jdbcTemplate.update(sql,dishName,dishId);
        }

        int updateDishPrice(int dishId , Double dishPrice)
        {
            String sql = "" +
                    "UPDATE dish " +
                    "SET dishPrice = ? " +
                    "WHERE dishId = ?";
            return jdbcTemplate.update(sql,dishPrice,dishId);
        }

    }
 */
}
