package com.marste.restaurant.Recipe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Patryk Markowski
 */

@Service
public class RecipeService
{
    private final RecipeDataAccessService recipeDataAccessService;

    @Autowired

    public RecipeService(RecipeDataAccessService recipeDataAccessService) {
        this.recipeDataAccessService = recipeDataAccessService;
    }
    void addRecipe (Recipe recipe)
    {
        recipeDataAccessService.insertRecipe(recipe);
    }
    List<Recipe> selectAllRecipes()
    {
        return recipeDataAccessService.selectAllRecipes();
    }
    List<Recipe> selectRecipesByDishName (String dishName)
    {
        return recipeDataAccessService.selectRecipeByDishName(dishName);
    }
    void deleteDishFromRecipes (String dishName)
    {
        recipeDataAccessService.deleteDishFromRecipes(dishName);
    }
    void deleteIngredientFromDishRecipe (String dishName , String ingredientName)
    {
        recipeDataAccessService.deleteIngredientFromDishRecipe(dishName,ingredientName);
    }

    public void updateRecipe(String dishName, String oldDishName) {
        recipeDataAccessService.updateRecipe(dishName,oldDishName);
    }
    /*
@Service
public class IngredientService {
    private final IngredientDataAccessService ingredientDataAccessService;

    @Autowired
    public IngredientService(IngredientDataAccessService ingredientDataAccessService) {
        this.ingredientDataAccessService = ingredientDataAccessService;
    }
    void addIngredient ( Ingredient ingredient)
    {
        ingredientDataAccessService.insertIngredient(ingredient);
    }
    List<Ingredient> selectAllIngredients ()
    {
       return ingredientDataAccessService.selectAllIngredients();
    }
    void deeteIngredientById (int id)
    {
        ingredientDataAccessService.deleteIngredientById(id);
    }
    void updateIngredient (Ingredient ingredient)
    {
        Ingredient oldIngredient = ingredientDataAccessService.selectIngredientById(ingredient.getIngredientId());
        if(!oldIngredient.getIngredientName().equals(ingredient.getIngredientName()))
        {
            ingredientDataAccessService.updateIngredientName(ingredient.getIngredientId(),ingredient.getIngredientName());
        }
    }
    Ingredient selectIngredientById(int id)
    {
        return ingredientDataAccessService.selectIngredientById(id);
    }
    Ingredient selectIngredientByName(String ingredientName)
    {
        return ingredientDataAccessService.selectIngredientByName(ingredientName);
    }*/
}
