package com.marste.restaurant.Recipe;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

/**
 * @author Patryk Markowski
 */
public class Recipe {
    @NotBlank
    private final String dishName;
    @NotBlank
    private final String ingredientName;



    public Recipe(
                  @JsonProperty("dishName") String dishName,
                  @JsonProperty("ingredientName") String ingredientName) {
        this.dishName = dishName;
        this.ingredientName = ingredientName;
    }

        public String getDishName() {
        return dishName;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "dishName='" + dishName + '\'' +
                ", ingredientName='" + ingredientName + '\'' +
                '}';
    }
}
