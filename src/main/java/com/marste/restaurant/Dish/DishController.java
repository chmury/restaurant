package com.marste.restaurant.Dish;

import com.marste.restaurant.User.UserController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Patryk Markowski, Mateusz Stępień
 */


/**
 * This class contains controller of class dish.
 * It provides API endpoints to maintain object "dish"
 */

@RequestMapping("api/dish")
@RestController
@CrossOrigin("*")
public class DishController {

    /**
     * This is private instance of dish service
     */
    private final DishService dishService;


    /**
     * This is constructos of dish controler with parameter.
     * @param dishService
     */
    @Autowired
    public DishController(DishService dishService) { this.dishService = dishService; }

    /**
     * This GET method loads list of all dishes from DB
     * @return List<Dish>
     */
    @GetMapping
    public List<Dish> getAllDish()
    {
        return  dishService.getAllDish(); }

    /**
     * This GET method loads single dish from DB.
     * @param dishId
     * @return Dish
     */
    @GetMapping(path = "{dishId}")
    public Dish getDishById(@PathVariable("dishId") int dishId)
    { return dishService.getDishById(dishId); }

    /**
     * This GET method loads single dish from DB.
     * @param dishName
     * @return Dish
     */
    @GetMapping( path = "/name/{dishName}")
    public Dish getDishByName(@PathVariable("dishName") String dishName)
    {
        return dishService.getDishByName(dishName);
    }

    /**
     * This GET method loads list of dishes of particular type from DB.
     * @param dishType
     * @return List<Dish>
     */
    @GetMapping(path = "/type/{dishType}")
    public List<Dish> getDishesByType(@PathVariable("dishType") String dishType)
    {
        return dishService.getDishByType(dishType);
    }

    /**
     * This POST method inserts new dish to DB.
     * @param dish
     */
    @PostMapping
    public void addNewDish (@RequestBody @Valid Dish dish)
    {
        dishService.addNewDish(dish);
    }

    /**
     * This DELETE method removes dish from DB.
     * @param dishId
     */
    @DeleteMapping(path = "/remove/{dishId}")
    public void deleteDish(@PathVariable("dishId") int dishId)
    {
        dishService.deleteDish(dishId);
    }

    /**
     * This POST method update dish in DB.
     * @param dish
     * @param dishName
     */
    @PostMapping(path = "/update/{dishName}")
    public void updateDish(@RequestBody Dish dish, @PathVariable("dishName") String dishName)
    {
        System.out.println(dish.toString());
        System.out.println(dishName);
        dishService.updateDish(dish,dishName);
    }

}
