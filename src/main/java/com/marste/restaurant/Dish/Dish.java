package com.marste.restaurant.Dish;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Patryk Markowski
 */
public class Dish {

    private Integer dishId;
    @NotBlank
    private final String dishName;
    @NotBlank
    private final Double dishPrice;
    @NotNull
    private final Dish.dishType dishType;

    public Dish(
            @JsonProperty("dishName") String name,
            @JsonProperty("dishPrice") Double price,
            @JsonProperty("dishType") Dish.dishType dishType) {

        this.dishName = name;
        this.dishPrice = price;
        this.dishType = dishType;
    }

    public Dish(Integer dishId, @NotBlank String dishName, @NotBlank Double dishPrice, @NotNull Dish.dishType dishType) {
        this.dishId = dishId;
        this.dishName = dishName;
        this.dishPrice = dishPrice;
        this.dishType = dishType;
    }

    public void setDishId(Integer dishId) {
        this.dishId = dishId;
    }

    public Integer getDishId() {
        return dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public Double getDishPrice() {
        return dishPrice;
    }

    public Dish.dishType getDishType() {
        return dishType;
    }

    public enum dishType {
        starter, pizza, pasta, sides, beverages, burgers, desserts
    }

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + dishId +
                ", name='" + dishName + '\'' +
                ", price=" + dishPrice +
                ", type=" + dishType +
                '}';
    }
}
