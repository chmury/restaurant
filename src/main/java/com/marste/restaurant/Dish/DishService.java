package com.marste.restaurant.Dish;

import com.marste.restaurant.Recipe.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Patryk Markowski
 */
@Service
public class DishService {

    private final DishDataAccessService dishDataAccessService;

    @Autowired
    public DishService(DishDataAccessService dishDataAccessService) { this.dishDataAccessService = dishDataAccessService; }

    void addNewDish (Dish dish)
    {
        dish.setDishId(findLastId()+1);
        dishDataAccessService.insertDish(dish);
    }

    List<Dish> getAllDish(){
        return  dishDataAccessService.selectAllDishes();

    }
    List<Dish> getDishByType(String dishType)
    {
        return dishDataAccessService.selectDishbyType(dishType);
    }

    void deleteDish(int id)
    {
        dishDataAccessService.deleteDishById(id);
    }

    public void updateDish(Dish dish, String dishName)
    {
        dishDataAccessService.updateDish(dish,dishName);


    }
    int findLastId(){
        return dishDataAccessService.findLastId();
    }
    Dish getDishById(int id)
    {
        return dishDataAccessService.selectDishById(id);
    }
    Dish getDishByName(String dishName)
    {
        return dishDataAccessService.selectDishByName(dishName);
    }
    List<Dish> getDishesByType (String dishType)
    {
        return dishDataAccessService.selectDishbyType(dishType);
    }

}
