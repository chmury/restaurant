package com.marste.restaurant.Dish;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.w3c.dom.events.EventException;

import java.util.List;

/**
 * @author Patryk Markowski
 */
@Repository
public class DishDataAccessService {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DishDataAccessService(JdbcTemplate jdbcTemplate) {this.jdbcTemplate = jdbcTemplate; }


    int findLastId(){
        try {
            String sql = "" +
                    "SELECT * FROM dish ORDER BY dishId DESC limit 1";
            List<Dish>list = jdbcTemplate.query(sql, mapDishFomDb());
            if(list.size()>0)
            {
                return list.get(0).getDishId();
            }
            else return 0;


        } catch (EventException e) {
            throw new RuntimeException(e);
        }

    }
    int insertDish(Dish dish)
    {
        String sql = "" +
                "INSERT INTO dish (" +
                " dishId, " +
                " dishName, " +
                " dishPrice, " +
                " dishType) "
                + "VALUES (?, ?, ?, ?::dishType)";
        return jdbcTemplate.update(
                sql,
                dish.getDishId(),
                dish.getDishName(),
                dish.getDishPrice(),
                dish.getDishType().name());

    }


     List<Dish> selectAllDishes() {
        String sql = "" +
                "SELECT " +
                " dishId, " +
                " dishName, " +
                " dishPrice, " +
                " dishType " +
                "FROM dish";

        return jdbcTemplate.query(sql, mapDishFomDb());
    }

    Dish selectDishById(int id)
    {
        String sql = "" +
                "SELECT " +
                " dishId, " +
                " dishName, " +
                " dishPrice, " +
                " dishType " +
                "FROM dish " +
                "WHERE dishId ="+id;

        return jdbcTemplate.queryForObject(sql,mapDishFomDb());
    }
    Dish selectDishByName(String dishName)
    {
        String sql = "" +
                "SELECT " +
                " dishId, " +
                " dishName, " +
                " dishPrice, " +
                " dishType " +
                "FROM dish " +
                "WHERE dishName ='"+dishName+"'";

        return jdbcTemplate.queryForObject(sql,mapDishFomDb());
    }
private RowMapper<Dish> mapDishFomDb() {
        return (resultSet, i) -> {
            int dishId = resultSet.getInt("dishId");
            String dishName = resultSet.getString("dishName");
            Double dishPrice = resultSet.getDouble("dishPrice");
            String dishTypeString = resultSet.getString("dishType");
            Dish.dishType dishType = Dish.dishType.valueOf(dishTypeString);
            return new Dish(
                    dishId,
                    dishName,
                    dishPrice,
                    dishType
            );
        };
    }

    int deleteDishById(int dishId) {
        String sql = "" +
                "DELETE FROM dish " +
                "WHERE dishId = ?";
        return jdbcTemplate.update(sql, dishId);
    }
    List<Dish> selectDishbyType(String dishType) {
        //TODO dopisać warunek żeby dishType był jednym z typów
        String sql = "" +
                "SELECT " +
                " dishId, " +
                " dishName, " +
                " dishPrice, " +
                " dishType " +
                "FROM dish " +
                "WHERE dishType = '" + dishType+"'";

        return jdbcTemplate.query(sql, mapDishFomDb());
    }



    int updateDish(Dish dish , String dishName)
    {
//
//        UPDATE dish
//        SET dishName = 'elo' , dishPrice = 999999 , dishType = 'pizza' WHERE dishName = 'qwesad'

        String sql = "" +
                "UPDATE dish " +
                "SET dishName = ? , " +
                "dishPrice = ? , " +
                "dishType = ?::dishType " +
                "WHERE dishName = ?";
        return jdbcTemplate.update(sql,dish.getDishName(),dish.getDishPrice(),dish.getDishType().name(),dishName);
    }

}
