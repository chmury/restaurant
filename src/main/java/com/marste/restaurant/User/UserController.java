package com.marste.restaurant.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author Patryk Markowski
 */
@RequestMapping("api/user")
@RestController
@CrossOrigin("*")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public void addUser (@RequestBody @Valid User user){userService.addUser(user);}
    @DeleteMapping(path = "/{userName}" )
    public void deleteUser(@PathVariable("userName") String userName){userService.deleteUSer(userName);}
    @PostMapping(path="/update")
    public void updateUserPassword (@RequestBody User user){
//        System.out.println(user.toString());
        userService.updateUser(user);
    }
//    @PutMapping("/authority")
//    public void updateUserAuthority(@RequestBody User user){userService.updateUserAuthority(user);}
    @GetMapping("/all")
    public List<User> getAllUsers() {return userService.getAllUsers();}
    @GetMapping(path = "/name/{userName}")
    public User getUserByName(@PathVariable("userName") String userName){
        return userService.getUserByName(userName);}
    @GetMapping(path = "/role/{userName}")
    public Map<String,String> getUserRoleByName(@PathVariable("userName") String userName){
        return userService.getUserRole(userName);}


}
