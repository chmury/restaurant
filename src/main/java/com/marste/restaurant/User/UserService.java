package com.marste.restaurant.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Patryk Markowski
 */
@Service
public class UserService {

    private UserDataAccessService userDataAccessService;

    @Autowired
    public UserService(UserDataAccessService userDataAccessService) {
        this.userDataAccessService = userDataAccessService;
    }

    @Autowired
    PasswordEncoder encoder;

    void addUser(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        userDataAccessService.insertUser(user);
    }

    List<User> getAllUsers() {
        return userDataAccessService.getAllUsers();
    }

    void deleteUSer(String userName) {
        userDataAccessService.deleteUser(userName);
    }

    void updateUser(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        userDataAccessService.updateUser(user);
    }

//    void updateUserAuthority(User user) {
//        userDataAccessService.updateUserAuthority(user);
//    }

    public Map<String, String> getUserRole(String userName) {
        HashMap<String, String> map = new HashMap<>();
        map.put("userRole", userDataAccessService.getUserRole(userName));
        return map;
    }

    public User getUserByName(String userName) {
        return userDataAccessService.getUserByName(userName);
    }
}
