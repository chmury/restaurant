package com.marste.restaurant.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Patryk Markowski
 */
@Repository
public class UserDataAccessService {
    private final JdbcTemplate jdbcTemplate;

    @Autowired

    public UserDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    int insertUser(User user) {
        String sql = "" +
                "INSERT into users (" +
                "userName, " +
                "password, " +
                "authority) "
                + "VALUES (?,?,?)";
        return jdbcTemplate.update(sql, user.getUserName(), user.getPassword(), user.getAuthority());
    }

    int deleteUser(String userName) {
        String sql = "" +
                "DELETE FROM users " +
                "WHERE userName = ?";
        return jdbcTemplate.update(sql, userName);
    }

    int updateUser(User user) {
        String sql = "" +
                "UPDATE users " +
                "SET password = ? , authority =?" +
                " WHERE userName = ?";
        return jdbcTemplate.update(sql, user.getPassword(), user.getAuthority(), user.getUserName());
    }

//    int updateUserAuthority(User user) {
//        String sql = "" +
//                "UPDATE users " +
//                "SET authority = ?" +
//                " WHERE userName = ?";
//        return jdbcTemplate.update(sql, user.getUserName(), user.getAuthority());
//    }

    private RowMapper<User> mapUserFromDb() {
        return (resultSet, i) -> {
            String userName = resultSet.getString("userName");
            String password = "NOTALLOWED";
            String authority = resultSet.getString("authority");
            return new User(
                    userName,
                    password,
                    authority
            );
        };
    }

    List<User> getAllUsers() {
        String sql = "" +
                "SELECT " +
                "userName, " +
                "password, " +
                "authority " +
                " FROM users";
        return jdbcTemplate.query(sql, mapUserFromDb());
    }

    User getUserByName(String userName) {
        String sql = "" +
                "SELECT " +
                "userName, " +
                "password, " +
                "authority " +
                "FROM users " +
                "WHERE userName = '" + userName + "'";
        return jdbcTemplate.queryForObject(sql, mapUserFromDb());
    }


    String getUserRole(String userName) {
        return getUserByName(userName).getAuthority();

    }


}
