package com.marste.restaurant.User;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.constraints.NotBlank;

/**
 * @author Patryk Markowski
 */
public class User {
    @Autowired
    PasswordEncoder passwordEncoder;

    @NotBlank
    private String userName;
    @NotBlank
    private String password;
    @NotBlank
    private String authority;

    public User(@JsonProperty("userName") String userName, @JsonProperty("password") String password,@JsonProperty("authority") String authority) {
        this.userName = userName;
        this.password = password;
        this.authority = authority;
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "User{" +

                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", authority='" + authority + '\'' +
                '}';
    }
}
