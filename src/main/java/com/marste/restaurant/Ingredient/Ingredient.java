package com.marste.restaurant.Ingredient;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

/**
 * @author Patryk Markowski
 */
public class Ingredient {

    private  Integer ingredientId;
    @NotBlank
    private  String ingredientName;

    public Ingredient( int ingredientId,
                       String ingredientName) {

        this.ingredientId = ingredientId;
        this.ingredientName = ingredientName;
    }

    public Ingredient( @JsonProperty("ingredientName")  String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public Integer getIngredientId() {
        return ingredientId;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "ingredientId=" + ingredientId +
                ", ingredientName='" + ingredientName + '\'' +
                '}';
    }
}
