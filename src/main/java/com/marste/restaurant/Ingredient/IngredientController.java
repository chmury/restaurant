package com.marste.restaurant.Ingredient;

import com.marste.restaurant.Dish.Dish;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Patryk Markowski Mateusz Stepien
 */

/**
 * This class contains controller of class ingredient.
 * It provides API endpoints to maintain object "ingredient"
 */
@RequestMapping("api/ingredient")
@RestController
@CrossOrigin("*")
public class IngredientController {
    private final IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }
    @GetMapping
    public List<Ingredient> getAllIngredients()
    {
        return  ingredientService.selectAllIngredients();
    }
    @GetMapping(path = "{ingredientId}")
    Ingredient selectIngredientById(@PathVariable("ingredientId") int ingredientId)
    {
        return ingredientService.selectIngredientById(ingredientId);
    }
    @GetMapping("/name")
    Ingredient selectIngredientByName( String ingredientName)
    {
        return ingredientService.selectIngredientByName(ingredientName);
    }
    @PostMapping
    public void addNewIngredient (@RequestBody @Valid Ingredient ingredient)
    {
        ingredientService.addIngredient(ingredient);
    }
    @DeleteMapping(path = "/remove/{ingredientId}")
    public void deleteIngredient(@PathVariable("ingredientId") int ingredientId)
    {
        ingredientService.deleteIngredientById(ingredientId);
    }

    @PutMapping
    public void updateStudent(@RequestBody Ingredient ingredient)
    {
        ingredientService.updateIngredient(ingredient);

    }
}
