package com.marste.restaurant.Ingredient;

import com.marste.restaurant.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.w3c.dom.events.EventException;

import java.util.List;

/**
 * @author Patryk Markowski
 */


@Repository
public class IngredientDataAccessService {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public IngredientDataAccessService(JdbcTemplate jdbcTemplate) { this.jdbcTemplate = jdbcTemplate; }


   int findLastId(){
       try {
           String sql = "" +
                   "SELECT * FROM ingredient ORDER BY ingredientId DESC limit 1";
           List<Ingredient>list= jdbcTemplate.query(sql, mapIngredientFomDb());
           if(list.size()>0){
               return list.get(0).getIngredientId();
           }
           return 0;


       } catch (DataAccessException e) {
           throw new RuntimeException(e);

       }



    }

    private RowMapper<Ingredient> mapIngredientFomDb() {
        return (resultSet, i) -> {
            int ingredientId = resultSet.getInt("ingredientId");
            String ingredientName = resultSet.getString("ingredientName");
            return new Ingredient(
                    ingredientId,
                    ingredientName
            );
        };
    }

    int insertIngredient(Ingredient ingredient) {
        String sql = "" +
                "INSERT INTO ingredient (" +
                " ingredientId, " +
                " ingredientName)" +
                "VALUES (?, ?)";
        return jdbcTemplate.update(
                sql,
                ingredient.getIngredientId(),
                ingredient.getIngredientName()
        );
    }

    int deleteIngredientById (int ingredientId) {
        String sql = "" +
                "DELETE FROM ingredient " +
                "WHERE ingredientId = ?";
        return jdbcTemplate.update(sql, ingredientId);
    }

    List<Ingredient> selectAllIngredients() {
        String sql = "" +
                "SELECT ingredientId, "+
                "ingredientName "+
                "FROM ingredient";
        return jdbcTemplate.query(sql, mapIngredientFomDb());

    }

    Ingredient selectIngredientById (int id)
    {
        String sql = "" +
                "SELECT ingredientId, "+
                "ingredientName "+
                "FROM ingredient "+
                "WHERE ingredientId = "+id;
        return jdbcTemplate.queryForObject(sql, mapIngredientFomDb());
    }

    Ingredient selectIngredientByName (String ingredientName)
    {
        String sql = "" +
                "SELECT ingredientId, "+
                "ingredientName "+
                "FROM ingredient "+
                "WHERE ingredientName = '"+ingredientName+"'";
        return jdbcTemplate.queryForObject(sql, mapIngredientFomDb());
    }

    int updateIngredientName(int ingredientId , String ingredientName)
    {
        String sql = "" +
                "UPDATE ingredient " +
                "SET ingredientName = ?"+
                " WHERE ingredientId = ?";
        return jdbcTemplate.update(sql,ingredientName,ingredientId);
    }

}
