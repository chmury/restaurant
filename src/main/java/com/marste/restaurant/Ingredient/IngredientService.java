package com.marste.restaurant.Ingredient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Patryk Markowski
 */
@Service
public class IngredientService {
    private final IngredientDataAccessService ingredientDataAccessService;

    @Autowired
    public IngredientService(IngredientDataAccessService ingredientDataAccessService) {
        this.ingredientDataAccessService = ingredientDataAccessService;
    }
    void addIngredient ( Ingredient ingredient)
    {
        ingredient.setIngredientId(findLastId()+1);

        List<Ingredient> all = ingredientDataAccessService.selectAllIngredients();
        for(Ingredient it:all)
        {
            if(ingredient.getIngredientName().equals(it.getIngredientName())){
                return;
            }
        }
        ingredientDataAccessService.insertIngredient(ingredient);
    }
    List<Ingredient> selectAllIngredients ()
    {
       return ingredientDataAccessService.selectAllIngredients();
    }
    void deleteIngredientById (int id)
    {
        ingredientDataAccessService.deleteIngredientById(id);
    }
    void updateIngredient (Ingredient ingredient)
    {
        Ingredient oldIngredient = ingredientDataAccessService.selectIngredientById(ingredient.getIngredientId());
        if(!oldIngredient.getIngredientName().equals(ingredient.getIngredientName()))
        {
            ingredientDataAccessService.updateIngredientName(ingredient.getIngredientId(),ingredient.getIngredientName());
        }
    }
    Ingredient selectIngredientById(int id)
    {
        return ingredientDataAccessService.selectIngredientById(id);
    }
    Ingredient selectIngredientByName(String ingredientName)
    {
        return ingredientDataAccessService.selectIngredientByName(ingredientName);
    }
    int findLastId(){
        return ingredientDataAccessService.findLastId();
    }

}
