package com.marste.restaurant.order;

import com.marste.restaurant.Dish.Dish;
import com.marste.restaurant.User.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Patryk Markowski
 */
@RequestMapping("api/order")
@RestController
@CrossOrigin("*")
public class OrderController {
    private final OrderService orderService;

    @Autowired

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/all")
    public List<Order> getAllActiveOrders() {return orderService.getAllActiveOrders();}

    @GetMapping("/allArchive")
    public List<Order> getAllArchiveOrders() {return orderService.getAllArchiveOrders();}

    @PostMapping
    public int addNewOrder(@RequestBody @Valid Order order)
    {
        order.setOrderId(orderService.findLastId()+1);
        orderService.addNewOrder(order);
        return order.getOrderId();
    }

    @GetMapping(path = "{orderId}")
    public Order getOrderById (@PathVariable("orderId") int orderId)
    { return orderService.getOrderById(orderId); }

    @RequestMapping(path = "/status/{id}/{newStatus}", method = RequestMethod.POST)
    public void changeOrderStatus(@PathVariable int id, @PathVariable String newStatus) {
        orderService.changeOrderStatus(id, newStatus);
    }

}
