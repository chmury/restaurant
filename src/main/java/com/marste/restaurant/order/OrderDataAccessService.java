package com.marste.restaurant.order;


import com.marste.restaurant.Recipe.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.w3c.dom.events.EventException;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Patryk Markowski
 */
@Repository
public class OrderDataAccessService {
    private final JdbcTemplate jdbcTemplate;

    @Autowired


    public OrderDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }




    public static class details {
        int order;
        int dish;

        public details(int order, int dish) {
            this.order = order;
            this.dish = dish;
        }

        public int getOrder() {
            return order;
        }

        public int getDish() {
            return dish;
        }
    }

    private RowMapper<Order> mapOrderFromDB() {
        return (resultSet, i) -> {
            int orderId = resultSet.getInt("orderId");
            String userName = resultSet.getString("userName");
            String orderDate = resultSet.getString("orderDate");
            String orderStatus = resultSet.getString("orderStatus");
            return new Order(
                    orderId,
                    userName,
                    orderDate,
                    orderStatus
            );
        };
    }

    public void changeOrderStatus(int id, String newStatus) {
        String sql = "" +
                "UPDATE activeorders " +
                "SET orderstatus = ?" +
                " WHERE orderid = ?";
         jdbcTemplate.update(sql, newStatus, id);
    }

    public void changeOrderStatusToCompleted (int id) {
        String sql = "" +
                "BEGIN TRANSACTION;\n" +
                "INSERT INTO archiveorders (orderid, username,orderdate, orderstatus)\n" +
                "SELECT *\n" +
                "FROM activeorders\n" +
                "WHERE orderid = " + id + ";" +
                "\n" +
                "DELETE FROM activeorders\n" +
                "WHERE orderid = " + id + ";" +
                "\n" +
                "COMMIT;";
        jdbcTemplate.update(sql);
    }

    public List<Order> getAllActiveOrders() {
        String sql = "" +
                "SELECT * FROM activeOrders";
        return jdbcTemplate.query(sql, mapOrderFromDB());
    }

    public List<Order> getAllArchiveOrders() {
        String sql = "" +
                "SELECT * FROM archiveOrders";
        return jdbcTemplate.query(sql, mapOrderFromDB());
    }

    List<details> getAllOrdersDetails() {
//        System.out.println("iddetails "+id);
        String sql = "" +
                "SELECT * FROM ordersDetails";

        return jdbcTemplate.query(sql, mapDetailsFromDB());
    }

    int insertOrderToActiveOrder(Order order) {
        String sql = "" +
                "INSERT INTO activeOrders (" +
                " orderId, " +
                " userName, " +
                " orderDate, " +
                "orderStatus)" +
                "VALUES (?, ?, ?, ?)";
        return jdbcTemplate.update(sql, order.getOrderId(), order.getUserName(), Date.valueOf(order.getOrderDate()), order.getOrderStatus());

    }

    int insertOrderToOrderDetails(Order order) {
        for (Integer it : order.getOrderedDishes()) {

//            System.out.println(it);
            String sql = "" +
                    "INSERT INTO ordersDetails (" +
                    " orderId, " +
                    "dishId) " +
                    "VALUES (?, ?)";
            jdbcTemplate.update(sql, order.getOrderId(), it);
        }
        return 0;
    }

    int findLastId() {
        int active = 0, archive = 0;
        try {
            String sql = "" +
                    "SELECT * FROM activeOrders ORDER BY orderId DESC limit 1";
            List<Order> a = jdbcTemplate.query(sql, mapOrderFromDB());
            if (a.size() > 0) {
                active = a.get(0).getOrderId();
            }
        } catch (EventException e) {
            System.out.println(e);
        }
        try {
            String sql = "" +
                    "SELECT * FROM archiveOrders ORDER BY orderId DESC limit 1";
            List<Order> a = jdbcTemplate.query(sql, mapOrderFromDB());
            if (a.size() > 0) {
                archive = a.get(0).getOrderId();
            }

        } catch (EventException e) {
            System.out.println(e);
        }
        if (active <= archive) return archive;
        return active;

    }


    private RowMapper<details> mapDetailsFromDB() {
        return (resultSet, i) -> {
            int orderId = resultSet.getInt("orderId");
            int dishId = resultSet.getInt("dishId");
            return new details(
                    orderId,
                    dishId
            );
        };
    }

    Order getActiveOrderById(int id) {
//        System.out.println("idactive "+id);
        String sql = "" +
                "SELECT * FROM activeOrders WHERE orderId = " + id;
        Order order = jdbcTemplate.queryForObject(sql, mapOrderFromDB());
        //brakuje listy dań
        order.setOrderedDishes(getOrderDetailsById(id));


        return order;
    }


    List<Integer> getOrderDetailsById(int id) {
//        System.out.println("iddetails "+id);
        String sql = "" +
                "SELECT * FROM ordersDetails WHERE orderId = " + id;
        List<details> a = jdbcTemplate.query(sql, mapDetailsFromDB());
        List<Integer> dishes = new ArrayList<>();
        for (details item : a) {
            dishes.add(item.getDish());
        }
        return dishes;
    }


}

//        private RowMapper<Order> mapOrderDetailsFromDB(){
//            return
//        }
//
//
//);

//        CREATE TABLE IF NOT EXISTS archiveOrders (
//                orderId int PRIMARY KEY NOT NULL,
//                userName VARCHAR(100) NOT NULL,
//        orderDate VARCHAR(100) NOT NULL,
//        orderStatus VARCHAR(25) NOT NUll
//);
//
//        CREATE TABLE IF NOT EXISTS ordersDetails (
//                orderId int  NOT NULL,
//                dishId int  NOT NULL,
//    }


/*


    @Repository
    public class IngredientDataAccessService {
        private final JdbcTemplate jdbcTemplate;

        @Autowired
        public IngredientDataAccessService(JdbcTemplate jdbcTemplate) { this.jdbcTemplate = jdbcTemplate; }




        private RowMapper<Ingredient> mapIngredientFomDb() {
            return (resultSet, i) -> {
                int ingredientId = resultSet.getInt("ingredientId");
                String ingredientName = resultSet.getString("ingredientName");
                return new Ingredient(
                        ingredientId,
                        ingredientName
                );
            };
        }



        int deleteIngredientById (int ingredientId) {
            String sql = "" +
                    "DELETE FROM ingredient " +
                    "WHERE ingredientId = ?";
            return jdbcTemplate.update(sql, ingredientId);
        }

        List<Ingredient> selectAllIngredients() {
            String sql = "" +
                    "SELECT ingredientId, "+
                    "ingredientName "+
                    "FROM ingredient";
            return jdbcTemplate.query(sql, mapIngredientFomDb());

        }

        Ingredient selectIngredientById (int id)
        {
            String sql = "" +
                    "SELECT ingredientId, "+
                    "ingredientName "+
                    "FROM ingredient "+
                    "WHERE ingredientId = "+id;
            return jdbcTemplate.queryForObject(sql, mapIngredientFomDb());
        }

        Ingredient selectIngredientByName (String ingredientName)
        {
            String sql = "" +
                    "SELECT ingredientId, "+
                    "ingredientName "+
                    "FROM ingredient "+
                    "WHERE ingredientName = '"+ingredientName+"'";
            return jdbcTemplate.queryForObject(sql, mapIngredientFomDb());
        }

        int updateIngredientName(int ingredientId , String ingredientName)
        {
            String sql = "" +
                    "UPDATE ingredient " +
                    "SET ingredientName = ?"+
                    " WHERE ingredientId = ?";
            return jdbcTemplate.update(sql,ingredientName,ingredientId);
        }

    }

*/
//}
