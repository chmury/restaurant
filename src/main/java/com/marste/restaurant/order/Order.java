package com.marste.restaurant.order;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Patryk Markowski
 */
public class Order {
    private Integer orderId;
    private String userName;
    private List<Integer> orderedDishes;
    private String orderDate;
    private String orderStatus;
    private Double orderValue;


    public Order( @JsonProperty("userName") String userName,@JsonProperty("orderedDishes") List<Integer> orderedDishes,@JsonProperty("orderValue") Double orderValue) {
//    public Order(@JsonProperty("orderId") Integer orderId, @JsonProperty("userName") String userName,@JsonProperty("orderedDishes") List<Integer> orderedDishes,@JsonProperty("orderValue") Double orderValue) {
//        this.orderId = orderId;
//        this.orderId = orderId;
        this.userName = userName;
        this.orderedDishes = orderedDishes;
        this.orderDate = LocalDate.now().toString(); //TODO FORMAT
        this.orderStatus = "Zamówione";
        this.orderValue = orderValue;
    }

    public Order(Integer orderId, String userName, String orderDate, String orderStatus) {
        this.orderId = orderId;
        this.userName = userName;
        this.orderDate = orderDate;
        this.orderStatus = orderStatus;
    }

    public Order(Integer orderId, String userName, List<Integer> orderedDishes, LocalDate orderDate, String orderStatus, Double orderValue) {
        this.orderId = orderId;
        this.userName = userName;
        this.orderedDishes = orderedDishes;
        this.orderDate = orderDate.toString();
        this.orderStatus = orderStatus;
        this.orderValue = orderValue;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Integer> getOrderedDishes() {
        return orderedDishes;
    }

    public void setOrderedDishes(List<Integer> orderedDishes) {
        this.orderedDishes = orderedDishes;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
