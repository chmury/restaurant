package com.marste.restaurant.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Patryk Markowski
 */
@Service
public class OrderService {


    private final OrderDataAccessService orderDataAccessService;

    @Autowired

    public OrderService(OrderDataAccessService orderDataAccessService) {
        this.orderDataAccessService = orderDataAccessService;
    }

    void addNewOrder(Order order) {

        orderDataAccessService.insertOrderToActiveOrder(order);
        orderDataAccessService.insertOrderToOrderDetails(order);
    }

    int findLastId() {
        return orderDataAccessService.findLastId();
    }

    Order getOrderById(int id) {
        return orderDataAccessService.getActiveOrderById(id);
    }

    public List<Order> getAllActiveOrders() {
        List<OrderDataAccessService.details> details = orderDataAccessService.getAllOrdersDetails();
        List<Order> orders = orderDataAccessService.getAllActiveOrders();

        for (int i = 0; i < orders.size(); i++) {
            List<Integer> dishes = new ArrayList<>();
            for (int j = 0; j < details.size(); j++) {
                if (details.get(j).order == orders.get(i).getOrderId()) {
                    dishes.add(details.get(j).dish);
                }


            }
            orders.get(i).setOrderedDishes(dishes);
//            System.out.println(orders.get(i).getOrderedDishes());
        }
//        for (int i = 0; i < orders.size(); i++) {
//            System.out.println(orders.get(i).getOrderedDishes());
//        }
        return orders;
    }

    public List<Order> getAllArchiveOrders() {
        List<OrderDataAccessService.details> details = orderDataAccessService.getAllOrdersDetails();
        List<Order> orders = orderDataAccessService.getAllArchiveOrders();

        for (int i = 0; i < orders.size(); i++) {
            List<Integer> dishes = new ArrayList<>();
            for (int j = 0; j < details.size(); j++) {
                if (details.get(j).order == orders.get(i).getOrderId()) {
                    dishes.add(details.get(j).dish);
                }

            }
            orders.get(i).setOrderedDishes(dishes);
        }

        return orders;
    }


    public void changeOrderStatus(int id, String newStatus) {
//        System.out.println(newStatus);
        if (newStatus.equals("Oplacone")) {
            orderDataAccessService.changeOrderStatus(id, newStatus);
            orderDataAccessService.changeOrderStatusToCompleted(id);
//            System.out.println("ten dobry");
        } else {
//            System.out.println("ten zly");
            orderDataAccessService.changeOrderStatus(id, newStatus);
        }

    }




    /*
    package com.marste.restaurant.Dish;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


    @Service
    public class DishService {

        private final DishDataAccessService dishDataAccessService;

        @Autowired
        public DishService(DishDataAccessService dishDataAccessService) { this.dishDataAccessService = dishDataAccessService; }

        void addNewDsih (Dish dish)
        {
            dishDataAccessService.insertDish(dish);
        }

        List<Dish> getAllDish(){
            return  dishDataAccessService.selectAllDishes();

        }
        List<Dish> getDishByType(String dishType)
        {
            return dishDataAccessService.selectDishbyType(dishType);
        }

        void deleteDish(int id)
        {
            dishDataAccessService.deleteDishById(id);
        }

        public void updateDish(Dish dish)
        {
            Dish oldDish = dishDataAccessService.selectDishById(dish.getDishId());
            if(!oldDish.getDishName().equals(dish.getDishName()))
            {
                dishDataAccessService.updateDishName(dish.getDishId(),dish.getDishName());
            }
            if(oldDish.getDishPrice()!=dish.getDishPrice())
            {
                dishDataAccessService.updateDishPrice(dish.getDishId(),dish.getDishPrice());
            }

        }
        Dish getDishById(int id)
        {
            return dishDataAccessService.selectDishById(id);
        }
        Dish getDishByName(String dishName)
        {
            return dishDataAccessService.selectDishByName(dishName);
        }
        List<Dish> getDishesByType (String dishType)
        {
            return dishDataAccessService.selectDishbyType(dishType);
        }

    }

    */
}
