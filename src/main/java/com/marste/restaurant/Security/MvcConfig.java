package com.marste.restaurant.Security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author Patryk Markowski
 */
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/js/**")
                .addResourceLocations("classpath:/js/**");
//
//        registry.addResourceHandler("/css/**")
//                .addResourceLocations("classpath:/css/**");
//        registry.addResourceHandler("/img/**")
//                .addResourceLocations("classpath:/img/**");
//        registry.addResourceHandler("/resources/**")
//                .addResourceLocations("classpath:/resources/**");
    }
}