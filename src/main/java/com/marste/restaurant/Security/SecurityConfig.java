package com.marste.restaurant.Security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;


/**
 * @author Patryk Markowski
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Autowired
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select username,password,active "
                        + "from users "
                        + "where username = ?")
                .authoritiesByUsernameQuery("select username,authority "
                        + "from users "
                        + "where username = ?")
                .passwordEncoder(passwordEncoder());


    }


//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/js/**","/oauth/token");
////        web.ignoring().antMatchers("/js/**","/oauth/token").anyRequest();
//    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests().antMatchers("/js/**","/resources/**").permitAll().anyRequest().permitAll();
//        http.authorizeRequests().antMatchers("/css/**", "/js/**", "/images/**").permitAll();
        http
                .authorizeRequests()
//                .antMatchers("/js/**","/resources/**").permitAll()
//                .antMatchers("/js/**").permitAll()

                .antMatchers("/css/**", "/js/**", "/images/**","/resources/**")
                .permitAll()
                .antMatchers("/oauth/token","/swagger-ui.html").permitAll()
//                .anyRequest().denyAll()  //nie działa js
//                .anyRequest().permitAll() //nie działa oauth


//                .and()
//                 .authorizeRequests()
//                .antMatchers("/css/**", "/js/**", "/images/**")
//                .permitAll()
//                .anyRequest()
//                .permitAll()


                .and()
                .formLogin()
                .disable();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}