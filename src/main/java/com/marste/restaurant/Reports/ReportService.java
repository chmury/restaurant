package com.marste.restaurant.Reports;

import com.marste.restaurant.Dish.Dish;
import com.marste.restaurant.order.Order;
import com.marste.restaurant.order.OrderDataAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ReportService {
    private final ReportDataAccessControl reportDataAccessControl;

    @Autowired

    public ReportService(ReportDataAccessControl reportDataAccessControl) {
        this.reportDataAccessControl = reportDataAccessControl;
    }

    List<Report> getAllReports() {
        List<Report> report = new ArrayList<>();
        List<Order> orders = findDetailsForOrders();

        orders = dateCutter(orders);
        Set<String> dates = new HashSet<String>(dateString(orders));
        for (String it : dates) {
            report.add(new Report(it));
        }


        return calculateData(orders, report);

    }

    Double orderValue(List<Integer> dishInt) {
        List<Dish> dishes = reportDataAccessControl.selectAllDishes();
        Double total = 0.0;
        for (Integer it : dishInt) {
            for (Dish dsh : dishes) {
                if (it == dsh.getDishId()) {
                    total += dsh.getDishPrice();
                }
            }
        }
        return total;
    }

    List<Report> calculateData(List<Order> orders, List<Report> reports) {

        for (Report it : reports) {
            Integer ordersAmount = 0;
            Integer dishesAmount = 0;
            Double totalOrdersValue = 0.0;


            for (Order ord : orders) {
                if (ord.getOrderDate().equals(it.getMonth())) {
                    ordersAmount++;
                    dishesAmount += ord.getOrderedDishes().size();
                    totalOrdersValue += orderValue(ord.getOrderedDishes());
                }

            }
            it.setDishesAmount(dishesAmount);
            it.setOrderAmount(ordersAmount);
            it.setValue(totalOrdersValue);


        }
        return reports;
    }

    List<String> dateString(List<Order> orders) {
        List<String> dates = new ArrayList<>();
        for (Order it : orders) {
            dates.add(it.getOrderDate());
        }
        return dates;
    }

    List<Order> dateCutter(List<Order> orders) {
        for (Order it : orders) {
            it.setOrderDate(it.getOrderDate().substring(0, 7));

        }
        return orders;
    }

    List<Order> findDetailsForOrders() {
        List<Order> orders = reportDataAccessControl.getAllArchiveOrders();
        List<OrderDataAccessService.details> details = reportDataAccessControl.getAllOrdersDetails();

        for (Order it : orders) {
            List<Integer> dishes = new ArrayList<>();
            for (int j = 0; j < details.size(); j++) {
                if (details.get(j).getOrder() == it.getOrderId()) {
                    dishes.add(details.get(j).getDish());

                }

            }
            it.setOrderedDishes(dishes);


        }
        return orders;
    }
}
