package com.marste.restaurant.Reports;

import com.marste.restaurant.Dish.Dish;
import com.marste.restaurant.order.Order;
import com.marste.restaurant.order.OrderDataAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReportDataAccessControl {
    private final JdbcTemplate jdbcTemplate;

    @Autowired

    public ReportDataAccessControl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    private RowMapper<OrderDataAccessService.details> mapDetailsFromDB() {
        return (resultSet, i) -> {
            int orderId = resultSet.getInt("orderId");
            int dishId = resultSet.getInt("dishId");
            return new OrderDataAccessService.details(
                    orderId,
                    dishId
            );
        };
    }
    private RowMapper<Dish> mapDishFomDb() {
        return (resultSet, i) -> {
            int dishId = resultSet.getInt("dishId");
            String dishName = resultSet.getString("dishName");
            Double dishPrice = resultSet.getDouble("dishPrice");
            String dishTypeString = resultSet.getString("dishType");
            Dish.dishType dishType = Dish.dishType.valueOf(dishTypeString);
            return new Dish(
                    dishId,
                    dishName,
                    dishPrice,
                    dishType
            );
        };
    }
    private RowMapper<Order> mapOrderFromDB() {
        return (resultSet, i) -> {
            int orderId = resultSet.getInt("orderId");
            String userName = resultSet.getString("userName");
            String orderDate = resultSet.getString("orderDate");
            String orderStatus = resultSet.getString("orderStatus");
            return new Order(
                    orderId,
                    userName,
                    orderDate,
                    orderStatus
            );
        };
    }
    List<Dish> selectAllDishes() {
        String sql = "" +
                "SELECT " +
                " dishId, " +
                " dishName, " +
                " dishPrice, " +
                " dishType " +
                "FROM dish";

        return jdbcTemplate.query(sql, mapDishFomDb());
    }

    public List<Order> getAllArchiveOrders() {
        String sql = "" +
                "SELECT * FROM archiveOrders";
        return jdbcTemplate.query(sql, mapOrderFromDB());
    }

    List<OrderDataAccessService.details> getAllOrdersDetails() {
//        System.out.println("iddetails "+id);
        String sql = "" +
                "SELECT * FROM ordersDetails";

        return jdbcTemplate.query(sql, mapDetailsFromDB());
    }


}
