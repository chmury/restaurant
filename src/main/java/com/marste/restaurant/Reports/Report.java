package com.marste.restaurant.Reports;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Report {
    private String month;
    private Integer orderAmount;
    private Integer dishesAmount;
    private Double value;

    public Report(String month, Integer orderAmount, Integer dishesAmount, Double value) {
        this.month = month;
        this.orderAmount = orderAmount;
        this.dishesAmount = dishesAmount;
        this.value = value;
    }

    public Report(String month) {
        this.month = month;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Integer orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getDishesAmount() {
        return dishesAmount;
    }

    public void setDishesAmount(Integer dishesAmount) {
        this.dishesAmount = dishesAmount;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
