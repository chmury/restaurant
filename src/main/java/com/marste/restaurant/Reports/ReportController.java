package com.marste.restaurant.Reports;

import com.marste.restaurant.Recipe.Recipe;
import com.marste.restaurant.Recipe.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/report")
@RestController
@CrossOrigin("*")
public class ReportController {

    private final ReportService reportService;

    @Autowired
    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }


    @RequestMapping( method= RequestMethod.GET, produces = { "application/xml", "text/xml" }, consumes = MediaType.ALL_VALUE )
    public List<Report> getAllReports() {
        return reportService.getAllReports();
    }
}