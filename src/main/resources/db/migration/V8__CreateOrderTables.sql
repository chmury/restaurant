CREATE TABLE IF NOT EXISTS activeOrders (
   orderId int PRIMARY KEY NOT NULL,
    userName VARCHAR(100) NOT NULL,
    orderDate VARCHAR(100) NOT NULL,
    orderStatus VARCHAR(25) NOT NUll
);

CREATE TABLE IF NOT EXISTS archiveOrders (
   orderId int PRIMARY KEY NOT NULL,
    userName VARCHAR(100) NOT NULL,
    orderDate VARCHAR(100) NOT NULL,
    orderStatus VARCHAR(25) NOT NUll
);

