CREATE TYPE dishType AS ENUM ('starter','pizza','pasta','sides','beverages','burgers','desserts');

ALTER TABLE dish
ALTER COLUMN dishType TYPE dishType
USING (dishType::dishType)