CREATE TABLE IF NOT EXISTS ingredient (
   ingredientId int PRIMARY KEY NOT NULL,
    ingredientName VARCHAR(100) NOT NULL
);