CREATE TABLE IF NOT EXISTS dish (
   dishId int PRIMARY KEY NOT NULL,
    dishName VARCHAR(100) NOT NULL,
    dishPrice DOUBLE PRECISION  NOT NULL,
    dishType VARCHAR (10) NOT NULL
        CHECK (
            dishType = 'starter'   OR
            dishType = 'pizza'   OR
            dishType = 'pasta' OR
            dishType = 'sides' OR
            dishType = 'beverages' OR
            dishType = 'burgers' OR
            dishType = 'desserts'

        )
);
